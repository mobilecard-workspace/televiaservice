<%@page import="java.io.PrintWriter"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
    <title>Purchase Verification</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="HandheldFriendly" content="true"/>
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
        root { 
            display: block;
        }

        html
        {
        	margin: 0;
            font-family: arial;
            font-size: 12px;
            font-weight: bold;
            color: white;
            background-color: #D3441C;
        }

        td{
        	font-family: arial;
            font-size: 12px;
        }

        .title
        {
        	font-family: arial;
        	font-weight: bold;
            font-size: 14px;

        }

        .title2
        {
        	font-family: arial;
            font-size: 12px;
        }

        input{
        	font-family: arial;
            width: 98%;
            height: 30px;
            font-size: 14px;    
        }                

        .styled-select select {
        	font-family: arial;
            width: 100%;
            padding: 5px;
            font-size: 14px;
            border: 1px solid #ccc;
            height: 35px;
        }
        
        .btn-style{
        	font-family: arial;
        	font-weight: bold;
        	width: 100%;
        	height: 40px;
        	opacity: 0.5;
			border : solid 1px #e6e6e6;
			border-radius : 3px;
			moz-border-radius : 3px;
			font-size : 16px;
			padding : 1px 17px;
			background-color : #ffffff;
		}

        /* CSS Document */		
    </style>      
    <script type="text/javascript">
		function sendform() {
			document.form1.Expires.value = document.getElementById("_cc_expmonth").value + "/" + document.getElementById("_cc_expyear").value;
			//alert ("Expires.value: " + document.form1.Expires.value);
			//document.form1.submit();
		}
	</script> 
</head>

<body style="width: 100%">

<center>
    <div class="title">
        Portal 3D Secure Dummy MobileCard - Diestel - Payworks
    </div>
</center>
<br/>
<form method="post" name="form1" autocomplete="off" action="/DiestelServices/fomularioRespuesta">
  
<%-- ------------------------------ Variables de Mas para el nuevo Procom ------------------------------- --%>

<%--  <% if(!(((int) (Math.random() * 10)) % 5 == 0)){%> 
 	<input type="hidden" name="EM_Response" value="approved"/>
 	<input type="hidden" name="EM_Auth" value="<%=(int) (Math.random() * 999999)  %> "/>
 <%}else{%>
 	 <input type="hidden" name="EM_Response" value="deny"/>
	 <input type="hidden" name="EM_Auth" value="000000"/>
 <% }%> --%>
 <input type="hidden" name="Reference3D" value="${prosa.orderId}"/>
 <input type="hidden" name="MerchantId" value="${prosa.merchant}"/>
 <input type="hidden" name="MerchantName" value="MOBILECARD"/>
 <input type="hidden" name="MerchantCity" value="Mexico"/>
 <input type="hidden" name="Cert3D" value="03"/>
 <input type="hidden" name="ForwardPath" value="${prosa.urlBack}" />
 <input type="hidden" name="Expires" value="" />
 <%-- <input type="hidden" name="email" value="${prosa.email}" /> --%>
<%-- --------------------------------------------------------------------- --%>
<center>
    <table border="0" width="250px">
        <thead>
        	<tr>
                <td class="title">
                    Por favor proporcione la siguiente informaci&oacute;n:
                </td>
            </tr>
            <tr>
                <td class="title2">
                    Informaci&oacute;n de la Tarjeta de Cr&eacute;dito.
                </td>
            </tr>
        </thead>
        <tbody>
        	<tr>
                <td>
                	Total:
                </td>
            </tr>
            <tr>
                <td>
                    <strong>${prosa.total} MX</strong>
                    <INPUT TYPE="hidden" NAME="Total" VALUE="${prosa.total}">
                </td>
            </tr>
            <%-- <tr>
                <td>
                    Nombre:
                </td>
            </tr>
            <tr>
                <td>
                    <INPUT TYPE="TEXT" NAME="cc_name" SIZE="40,1" MAXLENGTH="30" VALUE="" required="true">
                </td>
            </tr> --%>
            <tr>
                <td>
                    N&uacute;mero de Tarjeta:
                </td>
            </tr>
            <tr>
                <td>
                    <INPUT TYPE="TEXT" NAME="Card" MAXLENGTH="19" SIZE="40,1" VALUE="${prosa.tarjeta}" required="true">
                </td>
            </tr>
            <tr>
                <td>
                    Tipo:
                </td>
            </tr>
            <tr>
                <td>
                    <div class="styled-select">
                        <select name="CardType">
                        	<c:if test="${prosa.tipo eq 'VISA'}"> 
                        		<option value="VISA" selected >VISA</option>
                        	</c:if>
                        	<c:if test="${prosa.tipo != 'VISA'}"> 
                        		<option value="VISA"  >VISA</option>
                        	</c:if>
                        	
                        	<c:if test="${prosa.tipo eq 'MC'}"> 
                        		<option value="MC" selected >MasterCard</option>
                        	</c:if>
                        	<c:if test="${prosa.tipo != 'MC'}"> 
                        		<option value="MC"  >MasterCard</option>
                        	</c:if>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Fecha de vencimiento:
                </td>
            </tr>
            <tr>
                <td>
                    <div class="styled-select">
                        <select name="_cc_expmonth" id="_cc_expmonth">
                            <option  value="01">1</option>
                            <option  value="02">2</option>
                            <option  value="03">3</option>
                            <option  value="04">4</option>
                            <option  value="05">5</option>
                            <option  value="06">6</option>
                            <option  value="07">7</option>
                            <option  value="08">8</option>
                            <option  value="09">9</option>
                            <option  value="10">10</option>
                            <option  value="11">11</option>
                            <option  value="12">12</option>
                        </select>    
                    </div>
                    <div class="styled-select">
                        <select name="_cc_expyear" id="_cc_expyear">
                            <%
                                java.util.Calendar C= java.util.Calendar.getInstance();
                                int anio=C.get(java.util.Calendar.YEAR) - 2000;
                                //out.println("<option selected>"+anio+"</option>");
                                //anio++;
                                for(int i=0;i<15;i++)
                                {
                                	out.println("<option selected>"+anio+"</option>");
                                	anio++;
                                }
                            %>                
                        </select> 
					</div>
                </td>
            </tr>
            <tr>
                <td>
                    C&oacute;digo de seguridad (CVV2/CVC2):
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="cc_cvv2" size="3,1" maxlength="3" value="" required="true" />
                </td>
            </tr>
            <tr>
                <td>
                	</BR>
                    <input type="submit" value="Pagar" class="btn-style" onclick="sendform();"/>
                </td>
            </tr>
        </tbody>
    </table>
</center>
</form>
<script type="text/javascript">
	document.getElementById("_cc_expmonth").value = '${prosa.mes}';
	document.getElementById("_cc_expyear").value = ${prosa.anio};
	document.form1.Expires.value = document.getElementById("_cc_expmonth").value + "/" + document.getElementById("_cc_expyear").value;
</script> 
</body>
</html>

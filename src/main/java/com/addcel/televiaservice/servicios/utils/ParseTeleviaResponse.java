package com.addcel.televiaservice.servicios.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.addcel.televiaservice.servicios.model.vo.topup_service;
import com.addcel.televiaservice.servicios.model.vo.RespuestaTELEVIA;
import com.addcel.televiaservice.servicios.model.vo.balance_service;
import com.thoughtworks.xstream.XStream;

public class ParseTeleviaResponse {

	private static final Logger LOGGER = LoggerFactory.getLogger(ParseTeleviaResponse.class);
	
	private static XStream xStream;
        
        private static XStream xStreamaux;
	
	public topup_service parseResponseXStream(String cadena) {
                topup_service respTelevia = new topup_service();   
                //System.out.println("cadena : " + cadena);
		try {
                       respTelevia =  (topup_service) get().fromXML(cadena);                   
		} catch (Exception e) {
			LOGGER.error("ERROR AL DESERIALIZAR LA RESPUESTA DE TELEVIA RECARGA: "+e.getCause());
			e.printStackTrace();
		}
		return respTelevia;
	}
        
        public balance_service parseResponseXStreamSaldo(String cadena) {
                balance_service respTelevia = new balance_service();   
                System.out.println("cadena : " + cadena);
		try {
                       respTelevia =  (balance_service) getS().fromXML(cadena);                   
		} catch (Exception e) {
			LOGGER.error("ERROR AL DESERIALIZAR LA RESPUESTA DE TELEVIA SALDO: "+e.getCause());
			e.printStackTrace();
		}
		return respTelevia;
	}
	
	 private static XStream get() {
	        if (xStream == null) {
	            xStream = new XStream();
	            xStream.alias("topup_service", topup_service.class);
	        }
	        return xStream;
	    }

         private static XStream getS() {
	        if (xStreamaux == null) {
	            xStreamaux = new XStream();
	            xStreamaux.alias("balance_service", balance_service.class);
	        }
	        return xStreamaux;
	    }
	
	@Deprecated
	public RespuestaTELEVIA parseResponse(String cadena) {
		String sXML = cadena;
		RespuestaTELEVIA oR = new RespuestaTELEVIA();
		try {
			File file = new File("response.xml");
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			sXML = sXML.replace("<![CDATA[", "");
			sXML = sXML.replace("]]>", "");
			sXML = sXML.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
			sXML = sXML.replace("á", "a");
			sXML = sXML.replace("é", "e");
			sXML = sXML.replace("í", "i");
			sXML = sXML.replace("ó", "o");
			sXML = sXML.replace("ú", "u");

			sXML = sXML.replace("Á", "A");
			sXML = sXML.replace("É", "E");
			sXML = sXML.replace("í", "I");
			sXML = sXML.replace("Ó", "O");
			sXML = sXML.replace("Ú", "U");

			bw.write(sXML);
			bw.close();

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();

			NodeList Res = doc.getElementsByTagName("Topup_Service");
			Node firstPersonNode1 = Res.item(0);
			Node firstPersonNode2 = Res.item(0);
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("ID_GRP");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setId_grp(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode2.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode2;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("CARD_NUMBER");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setCard_number(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("CHECK_DIGIT");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setCheck_digit(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("LOCAL_DATE");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setLocal_date(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("AMOUNT");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setAmount(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("TRX_NO");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setTrx_no(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("AUTONO");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setAutono(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("RESPONSECODE");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setResponsecode(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("DESCRIPTIONCODE");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setDescriptioncode(textFNList.item(0).getNodeValue().trim());
				}
			}
			
		} catch (Exception ex) {
			LOGGER.debug("ERROR [PARCEXML IAVE: ]: " + ex.toString());
			oR.setResponsecode("9999");
			oR.setDescriptioncode("ERROR- Banco (1)");
			
		}
		return oR;
	}
	
}

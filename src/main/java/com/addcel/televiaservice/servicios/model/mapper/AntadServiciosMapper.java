package com.addcel.televiaservice.servicios.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.televiaservice.servicios.model.vo.AfiliacionVO;
import com.addcel.televiaservice.servicios.model.vo.AntadDetalle;
import com.addcel.televiaservice.servicios.model.vo.AntadInfo;
import com.addcel.televiaservice.servicios.model.vo.BitacoraDetalleVO;
import com.addcel.televiaservice.servicios.model.vo.BitacoraVO;
import com.addcel.televiaservice.servicios.model.vo.Categorias;
import com.addcel.televiaservice.servicios.model.vo.DatosDevolucion;
import com.addcel.televiaservice.servicios.model.vo.RecargaMontos;
import com.addcel.televiaservice.servicios.model.vo.Recargas;
import com.addcel.televiaservice.servicios.model.vo.RuleVO;
import com.addcel.televiaservice.servicios.model.vo.ServiciosAntadRequest;
import com.addcel.televiaservice.servicios.model.vo.ServiciosAntadResponse;
import com.addcel.televiaservice.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.televiaservice.servicios.model.vo.TBitacoraVO;
import com.addcel.televiaservice.servicios.model.vo.TeleviaDetalle;
import com.addcel.televiaservice.servicios.model.vo.TeleviaInfo;
import com.addcel.televiaservice.servicios.model.vo.UsuarioVO;

public interface AntadServiciosMapper {

	public List<ServiciosAntadResponse> consultaServiciosAntad(ServiciosAntadRequest request);

	public int insertaBitacoraTransaccion(BitacoraVO bitacora);
	
	public int insertaBitacoraTransaccionDetalle(BitacoraDetalleVO bitacora);

	public double getComision(int idProveedor);

	public void actualizaBitacora(BitacoraVO bitacora);

	public UsuarioVO getUsuario(@Param(value = "id") long idUsuario);
	
	public String getFechaActual();

	public void insertaTeleviaDetalle(TeleviaDetalle televiaDetalle);

	public void insertaBitacoraProsa(BitacoraVO bitacora);
	
	public int addBitacoraProsa(TBitacoraProsaVO b);

	public List<Recargas> getRecargas(int idAplicacion);

	public List<Recargas> getRecargaServicios(int idAplicacion);

	public List<RecargaMontos> getRecargaMontos(int idServicio);

	public List<Categorias> getServicioCategorias(int idAplicacion);

	public AfiliacionVO buscaAfiliacion(@Param(value="id") String id);
	
	public int guardaTBitacoraPIN(@Param(value="idBitacora") long idBitacora, @Param(value="pin") String pin);
	
	public String buscarTBitacoraPIN(@Param(value="idBitacora")String idBitacora);
	
	public int updateBitacora(TBitacoraVO b);
	
	public int updateBitacoraProsa(TBitacoraProsaVO b);
	
	public TeleviaInfo buscarDetallePago(@Param(value="idBitacora") String idBitacora);
	
	public void insertaDatosDevolucion(DatosDevolucion devolucion);
	
        public long getMaxIdOrder();
        
	public DatosDevolucion getDatosDevolucion(@Param(value="idBitacora") long idBitacora);

	public String getParametro(@Param(value = "clave") String valor);
	
	public RuleVO selectRule(@Param(value = "idUsuario") long idUsuario, @Param(value = "tarjeta") String tarjeta,
			@Param(value = "idProveedores") String idProveedores, @Param(value = "parametro") String parametro);
	
	public int getBloqueoTarjeta(@Param(value = "tarjeta") String tarjeta);
	
}

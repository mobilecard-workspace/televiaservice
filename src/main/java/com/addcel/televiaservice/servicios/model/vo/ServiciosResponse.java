package com.addcel.televiaservice.servicios.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ServiciosResponse extends AbstractVO {

	private List<ServiciosAntadResponse> servicios;

	public List<ServiciosAntadResponse> getServicios() {
		return servicios;
	}

	public void setServicios(List<ServiciosAntadResponse> servicios) {
		this.servicios = servicios;
	}
	
}

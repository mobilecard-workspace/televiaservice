package com.addcel.televiaservice.servicios.model.mapper;

import org.apache.ibatis.annotations.Param;

import com.addcel.televiaservice.servicios.model.vo.AfiliacionVO;
import com.addcel.televiaservice.servicios.model.vo.Producto;
import com.addcel.televiaservice.servicios.model.vo.Proveedor;
import com.addcel.televiaservice.servicios.model.vo.topup_service;
import com.addcel.televiaservice.servicios.model.vo.RuleVO;
import com.addcel.televiaservice.servicios.model.vo.TBitTeleviaDet;
import com.addcel.televiaservice.servicios.model.vo.TBitacoraVO;
import com.addcel.televiaservice.servicios.model.vo.UsuarioVO;
import com.addcel.televiaservice.servicios.model.vo.topup_service;

public interface TeleviaMapper {

	public UsuarioVO validaUsuario(@Param(value = "login") String login);
	
	public String getParametro(@Param(value = "clave") String valor);
	
        public long getMaxIdOrder();
        
	public String getComision(@Param(value = "clave") String valor);

	public RuleVO selectRule(@Param(value = "idUsuario") long idUsuario, @Param(value = "tarjeta") String tarjeta,
			@Param(value = "idProveedores") String idProveedores, @Param(value = "parametro") String parametro);

	public Producto getProductoByClaveWS(@Param(value = "producto") String producto);

	public double getComisionXProducto(@Param(value = "proveedor") int proveedor);

	public Proveedor getProveedor(@Param(value = "proveedor") int idProveedor);

	public void addBitacora(TBitacoraVO bitacora);

	public int getInfoTag(@Param(value = "tag") String tarjeta);

	public int getInfoImei(@Param(value = "imei") String imei);

	public void addBitacoraTELEVIA(TBitTeleviaDet bitacora);
	
	public String getFolioBancoTELEVIA();

	public void addBitacoraProsa(TBitacoraVO bitacora);
	
	public TBitacoraVO getDatosTransaccion(@Param(value = "idBitacora") long idBitacora);

	public void actualizaBitacora(TBitacoraVO bitacora);

	public void actualizaBitacoraProsa(TBitacoraVO bitacora);

	public void actualizaBitacoraTELEVIA(TBitTeleviaDet response); //pendiente
	
	public AfiliacionVO buscaAfiliacion(@Param(value="id") String id);
	
}

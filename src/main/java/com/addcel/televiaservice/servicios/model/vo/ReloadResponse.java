package com.addcel.televiaservice.servicios.model.vo;

public class ReloadResponse {

    private String id_order;
    private String id_tag;
    private String amount;
    private String balance;
    private String seq;
    private String ret_code;
    private String ret_msg;
    
    private long idBitacora;

    public ReloadResponse() {
    }

    public ReloadResponse(String id_order, String id_tag, String amount, 
                          String balance, String seq, String ret_code, String ret_msg) {
        this.id_order = id_order;
        this.id_tag = id_tag;
        this.amount = amount;
        this.balance = balance;
        this.seq = seq;
        this.ret_code = ret_code;
        this.ret_msg = ret_msg;
    }

       public String getId_order() {
        return id_order;
    }

    public void setId_order(String id_order) {
        this.id_order = id_order;
    }

    public String getId_tag() {
        return id_tag;
    }

    public void setId_tag(String id_tag) {
        this.id_tag = id_tag;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getRet_code() {
        return ret_code;
    }

    public void setRet_code(String ret_code) {
        this.ret_code = ret_code;
    }

    public String getRet_msg() {
        return ret_msg;
    }

    public void setRet_msg(String ret_msg) {
        this.ret_msg = ret_msg;
    }

    public long getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(long idBitacora) {
        this.idBitacora = idBitacora;
    }
	
}

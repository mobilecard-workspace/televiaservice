/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.televiaservice.servicios.model.vo;

/**
 *
 * @author Addcel
 */
public class balance_service {
    private String id_tag;
    private String balance;
    private String id_account;
    private String seq;
    private String ret_code;
    private String ret_msg;
    
    private long idBitacora;
    
    public balance_service() {     
    }
    
    public balance_service(String id_tag, String balance, String id_account, String seq,
                          String ret_code, String ret_msg) {
        this.id_tag = id_tag;
        this.balance = balance;
        this.id_account = id_account;
        this.seq = seq;
        this.ret_code = ret_code;
        this.ret_msg = ret_msg;
    }

    public String getId_tag() {
        return id_tag;
    }
    
    public String getBalance() {
        return balance;
    }
    
    public String getId_account() {
        return id_account;
    }
    
    public String getSeq() {
        return seq;
    }
    
    public String getRet_code() {
        return ret_code;
    }
    
     public String getRet_msg() {
        return ret_msg;
    }
    
    public long getIdBitacora() {
        return idBitacora;
    }

    public void setId_tag(String id_tag) {
        this.id_tag = id_tag;
    }
    
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public void setId_account(String id_account) {
        this.id_account = id_account;
    }
    
    public void setSeq(String seq) {
        this.seq = seq;
    }

    public void setRet_code(String ret_code) {
        this.ret_code = ret_code;
    }

    public void setRet_msg(String ret_msg) {
        this.ret_msg = ret_msg;
    }

    public void setIdBitacora(long idBitacora) {
        this.idBitacora = idBitacora;
    }
}

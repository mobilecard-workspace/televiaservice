package com.addcel.televiaservice.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class MobilePaymentRequestVO extends ResponseConsultaLCVO{
	
	private long idTransaccion;
	
	private String email;
	
	private String firmaBase64;
	
	private String nombre;
	
	private int tipoTarjeta;
	
	private String tarjeta;
	
	private String vigencia;
	
	private String mes;
	
	private String anio;
	
	private String cvv2;
	
	private String imei;
	
	private String tipo;
	
	private String software;
	
	private String modelo;
	
	private AfiliacionVO afiliacion;
	
	private String referencia;
	
	private String idUsuario;
	
	private String concepto;
	
	private String idProveedor;
	
	private int emisor;
	
	private String operacion;
	
	private double comision;
	
	private String tarjetaT;
	
	public MobilePaymentRequestVO() {	
	}
	
	public MobilePaymentRequestVO(int idError, String mensajeError) {	
		super(idError, mensajeError);
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirmaBase64() {
		return firmaBase64;
	}
	public void setFirmaBase64(String firmaBase64) {
		this.firmaBase64 = firmaBase64;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public int getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public AfiliacionVO getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(AfiliacionVO afiliacion) {
		this.afiliacion = afiliacion;
	}
	
	public String toTrace(){
		return "[ID BITACORA: "+idTransaccion
				+", NOMBRE: "+nombre
				+", TIPO TARJETA: "+tipoTarjeta+"]";
//				+", AFILIACION: "+afiliacion.toString()+"]";
	}

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}

	public int getEmisor() {
		return emisor;
	}

	public void setEmisor(int emisor) {
		this.emisor = emisor;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getTarjetaT() {
		return tarjetaT;
	}

	public void setTarjetaT(String tarjetaT) {
		this.tarjetaT = tarjetaT;
	}
	
}

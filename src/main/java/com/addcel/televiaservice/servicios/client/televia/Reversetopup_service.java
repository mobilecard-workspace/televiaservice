//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.05.27 a las 02:06:48 PM CDT 
//


package com.addcel.televiaservice.servicios.client.televia;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReverseReloadResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reverseTopup_Service"
})
@XmlRootElement(name = "Reversetopup_service")
public class Reversetopup_service {

    @XmlElement(name = "Reversetopup_service")
    protected String reverseTopup_Service;

    /**
     * Obtiene el valor de la propiedad reverseReloadResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTopup_Service() {
        return reverseTopup_Service;
    }

    /**
     * Define el valor de la propiedad reverseReloadResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReverseTopup_Service(String value) {
        this.reverseTopup_Service = value;
    }

}

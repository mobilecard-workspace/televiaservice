/**
 * BalanceServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.televiaservice.servicios.client.televia;

public interface BalanceServiceService extends javax.xml.rpc.Service {
    public java.lang.String getBalanceServiceAddress();

    public com.addcel.televiaservice.servicios.client.televia.BalanceService getBalanceService() throws javax.xml.rpc.ServiceException;

    public com.addcel.televiaservice.servicios.client.televia.BalanceService getBalanceService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

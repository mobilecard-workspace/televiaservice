/**
 * TopupService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.televiaservice.servicios.client.televia;

public interface TopupService extends java.rmi.Remote {
    public java.lang.String doTopupService(java.lang.String xmlInputData) throws java.rmi.RemoteException;
}

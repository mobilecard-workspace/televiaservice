//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.05.27 a las 02:06:48 PM CDT 
//


package com.addcel.televiaservice.servicios.client.televia;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the iave.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: iave.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Reload }
     * 
     */
    public Reload createReload() {
        return new Reload();
    }

    /**
     * Create an instance of {@link Topup_Service }
     * 
     */
    public topup_service createTopup_Service() {
        return new topup_service();
    }

    /**
     * Create an instance of {@link ReverseReload }
     * 
     */
    public ReverseReload createReverseReload() {
        return new ReverseReload();
    }

    /**
     * Create an instance of {@link ReverseTopup_Service }
     * 
     */
    public Reversetopup_service createReverseTopup_Service() {
        return new Reversetopup_service();
    }

}

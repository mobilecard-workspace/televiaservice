/**
 * BalanceServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.televiaservice.servicios.client.televia;

public class BalanceServiceServiceLocator extends org.apache.axis.client.Service implements com.addcel.televiaservice.servicios.client.televia.BalanceServiceService {

    public BalanceServiceServiceLocator() {
    }


    public BalanceServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BalanceServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BalanceService
    private java.lang.String BalanceService_address = "https://www.televia.mx/SmsServices/services/BalanceService";

    public java.lang.String getBalanceServiceAddress() {
        return BalanceService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BalanceServiceWSDDServiceName = "BalanceService";

    public java.lang.String getBalanceServiceWSDDServiceName() {
        return BalanceServiceWSDDServiceName;
    }

    public void setBalanceServiceWSDDServiceName(java.lang.String name) {
        BalanceServiceWSDDServiceName = name;
    }

    public com.addcel.televiaservice.servicios.client.televia.BalanceService getBalanceService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BalanceService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBalanceService(endpoint);
    }

    public com.addcel.televiaservice.servicios.client.televia.BalanceService getBalanceService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.televiaservice.servicios.client.televia.BalanceServiceSoapBindingStub _stub = new com.addcel.televiaservice.servicios.client.televia.BalanceServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getBalanceServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBalanceServiceEndpointAddress(java.lang.String address) {
        BalanceService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.televiaservice.servicios.client.televia.BalanceService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.televiaservice.servicios.client.televia.BalanceServiceSoapBindingStub _stub = new com.addcel.televiaservice.servicios.client.televia.BalanceServiceSoapBindingStub(new java.net.URL(BalanceService_address), this);
                _stub.setPortName(getBalanceServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BalanceService".equals(inputPortName)) {
            return getBalanceService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services.smsservices.indra.es", "BalanceServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services.smsservices.indra.es", "BalanceService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BalanceService".equals(portName)) {
            setBalanceServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

/**
 * TopupServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.televiaservice.servicios.client.televia;

public interface TopupServiceService extends javax.xml.rpc.Service {
    public java.lang.String getTopupServiceAddress();

    public com.addcel.televiaservice.servicios.client.televia.TopupService getTopupService() throws javax.xml.rpc.ServiceException;

    public com.addcel.televiaservice.servicios.client.televia.TopupService getTopupService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

package com.addcel.televiaservice.servicios.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.televiaservice.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.televiaservice.servicios.service.PayworksService;

@Controller
public class PayworksController {
	
	@Autowired
	private PayworksService parworksService;
	
	
	@RequestMapping(value = "/pagoTelevia3DS")
	public ModelAndView procesaPago(@RequestParam("json") String data,  ModelMap modelo) {
                //LOGGER.info("PAGO: "+pago.toTrace());
		return parworksService.procesaPago(data, modelo);
	}
	
	@RequestMapping(value = "/enviaPago3DS", method = RequestMethod.POST)
	public ModelAndView enviaPago3DS(@ModelAttribute MobilePaymentRequestVO pago,  ModelMap modelo) {
		return parworksService.procesaPago(pago, modelo);
	}
	
	
	@RequestMapping(value = "/payworksRec3DRespuesta")
	public ModelAndView payworksRec3DRespuesta(@RequestBody String cadena, ModelMap modelo) {
		return parworksService.procesaRespuesta3DPayworks(cadena, modelo);	
	}
	
	@RequestMapping(value = "/payworks2RecRespuesta")
	public ModelAndView payworks2RecRespuesta(
			@RequestParam String NUMERO_CONTROL,
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam(required = false) String CODIGO_PAYW, @RequestParam(required = false) String RESULTADO_AUT,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW, @RequestParam(required = false) String BANCO_EMISOR,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION, @RequestParam(required = false) String TIPO_TARJETA, @RequestParam(required = false) String MARCA_TARJETA, 
			ModelMap modelo,HttpServletRequest request) {
		return parworksService.payworks2Respuesta(NUMERO_CONTROL,REFERENCIA, FECHA_RSP_CTE, 
				TEXTO, RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT, CODIGO_PAYW, RESULTADO_AUT, 
				BANCO_EMISOR, ID_AFILIACION, TIPO_TARJETA, MARCA_TARJETA, modelo, request);	
	}
	
	//REFERENCIA=460277892863&
	//FECHA_RSP_CTE=20160503%2014:25:38.807&
	//TEXTO=Aprobada+por+Payworks+%28modo+prueba%29&
	//RESULTADO_PAYW=A&
	//FECHA_REQ_CTE=20160503%2014:25:38.792&
	//CODIGO_AUT=868245&
	//ID_AFILIACION=7495988
	//156116
	@RequestMapping(value = "/payworksDevoRespuesta")
	public ModelAndView payworksDevoRespuesta(
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION,
			ModelMap modelo,HttpServletRequest request) {
		return parworksService.payworks2DevoRespuesta(REFERENCIA, FECHA_RSP_CTE, 
				TEXTO, RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT,
				ID_AFILIACION, modelo, request);	
	}
	
}

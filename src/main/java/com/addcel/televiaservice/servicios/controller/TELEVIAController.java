package com.addcel.televiaservice.servicios.controller;

import com.addcel.televiaservice.servicios.model.vo.balance_service;
import com.addcel.televiaservice.servicios.model.vo.topup_service;
import com.addcel.televiaservice.servicios.service.PayworksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.televiaservice.servicios.utils.Utils;
import com.addcel.televiaservice.servicios.service.TELEVIAService;
import com.addcel.televiaservice.servicios.utils.ParseTeleviaResponse;
import com.addcel.utils.AddcelCrypto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import com.addcel.televiaservice.servicios.utils.HttpsUtils;

@Controller
public class TELEVIAController {
        
        private static final Logger LOGGER = LoggerFactory.getLogger(TELEVIAController.class);
    
	private static final String PATH_PAGO_TELEVIA = "/televia/3dsecure/procesaPago";
	
	private static final String PATH_PROCESA_RESPUESTA_PROSA = "/televia/3dsecure/respuestaProsa";
	
        private static final String PATH_CONSULTA_SALDO = "/televia/3dsecure/consultaSaldo";
        
	private static final String PARAM_JSON = "json";
	
	@Autowired
	private TELEVIAService televiaService;
	
	@RequestMapping(value = PATH_PAGO_TELEVIA, method = RequestMethod.POST)
	public ModelAndView pagoTelevia3DS(@RequestParam(PARAM_JSON) String json) {
                LOGGER.info("json contiene : "+ AddcelCrypto.decryptSensitive(json));
		return televiaService.pagoTelevia3DS(json);
	}
	
	@RequestMapping(value = PATH_PROCESA_RESPUESTA_PROSA, method = RequestMethod.POST)
	public ModelAndView respuestaProsa(@RequestParam String EM_Response,
			@RequestParam String EM_Total, @RequestParam String EM_OrderID,
			@RequestParam String EM_Merchant, @RequestParam String EM_Store,
			@RequestParam String EM_Term, @RequestParam String EM_RefNum,
			@RequestParam String EM_Auth, @RequestParam String EM_Digest) {
		
		return televiaService.procesaRespuestaProsa(EM_Response,
				EM_Total, EM_OrderID, EM_Merchant, EM_Store,
				EM_Term, EM_RefNum, EM_Auth, EM_Digest);	
	}
		
        @RequestMapping(value = PATH_CONSULTA_SALDO, method = RequestMethod.POST)
	public void consultaSaldo(){
            String res = "";
		StringBuffer pXML = new StringBuffer().append("<balance_service><request>").append("<user>").append("mobilecardmx")
				.append("</user>").append("<password>").append("m0c4Rmx01pwd").append("</password>")
				.append("<id_tag>").append("09900032558").append("</id_tag>").append("</request></balance_service>");
		System.out.println("MPX COMPRA IAVE : " + pXML);
                com.addcel.televiaservice.servicios.client.televia.BalanceServiceSoapBindingStub binding = null;
                
	        /*res = HttpsUtils.peticionHttpsUrlParams("https://www.televia.mx/SmsServices/SmsServices/consultaSaldo.do", "xml=<?xml version=\"1.0\" encoding=\"UTF-8\"?> <balance_service>   <request>     <user>mobilecardmx</user>     <password>m0c4Rmx01pwd</password>     <id_tag>09900032558</id_tag>   </request>   <response>     <user>smsservice</user>     <password>smsservicepwd</password>     <balance>8</balance>     <id_account>0000016220</id_account>     <seq>4253806</seq>     <ret_code>0</ret_code>     <ret_msg />   </response> </balance_service>");
                System.out.println("Resultado consulta saldo :" + res);*/
                try {
                        binding = (com.addcel.televiaservice.servicios.client.televia.BalanceServiceSoapBindingStub)
	                          new com.addcel.televiaservice.servicios.client.televia.BalanceServiceServiceLocator().getBalanceService();
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
                try
		{
		    System.out.println("Llego 1 ");
		    res = binding.doCheckBalance(pXML.toString());
		    System.out.println("Respuesta :  " + res);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error : " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException: " + re);
		}
                topup_service respuesta = new topup_service();
                balance_service respuesta1 = new balance_service();
                String resxml = "<topup_service><request><user>mobilecardmx</user><password>m0c4Rmx01pwd</password><id_order>100000002</id_order><id_tag>09900032558</id_tag><amount>1</amount></request><response><user>smsservice</user><password>smsservicepwd</password><balance>8</balance><seq>15575948</seq><ret_code>0</ret_code><ret_msg/></response></topup_service>";
                String s14 = Utils.transformarXMLRes(resxml);
                ParseTeleviaResponse parseTelevia = new ParseTeleviaResponse();
			respuesta = parseTelevia.parseResponseXStream(s14);
                        System.out.println("respuesta.getRet_code() : " + respuesta.getRet_code());
                        System.out.println("respuesta.getBalance() : " + respuesta.getBalance());
                /*proceso para consulta de saldo*/        
                String resxml1 = "<balance_service><request><user>mobilecardmx</user><password>m0c4Rmx01pwd</password><id_tag>09900032558</id_tag></request><response><user>smsservice</user><password>smsservicepwd</password><balance>11</balance><id_account>0000016220</id_account><seq>4305322</seq><ret_code>0</ret_code><ret_msg/></response></balance_service>";
                String s15 = Utils.transformarXMLRes(resxml1);        
                ParseTeleviaResponse parseTelevia1 = new ParseTeleviaResponse();
			respuesta1 = parseTelevia1.parseResponseXStreamSaldo(s15);
                        System.out.println("Saldo : " + respuesta1.getBalance());
                        System.out.println("Código de respuesta : " + respuesta1.getRet_code());        
        }
}
package com.addcel.televiaservice.servicios.service;

import com.addcel.televiaservice.servicios.model.mapper.AntadServiciosMapper;
import static com.addcel.televiaservice.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;
import static com.addcel.televiaservice.servicios.utils.Constantes.JSON_ERROR_INESPERADO;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_ERROR_PROCESA_PAGO;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_RESPUESTA_PROCESA_PAGO;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_SERVICE;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.addcel.televiaservice.servicios.client.antad.ObjectFactory;
import com.addcel.televiaservice.servicios.client.antad.ProcesaAutorizacion;
import com.addcel.televiaservice.servicios.client.antad.ProcesaAutorizacionResponse;
import com.addcel.televiaservice.servicios.client.televia.Reload;
import com.addcel.televiaservice.servicios.model.mapper.AntadServiciosMapper;
import com.addcel.televiaservice.servicios.model.vo.AbstractVO;
import com.addcel.televiaservice.servicios.model.vo.AfiliacionVO;
import com.addcel.televiaservice.servicios.model.vo.AntadDetalle;
import com.addcel.televiaservice.servicios.model.vo.AntadInfo;
import com.addcel.televiaservice.servicios.model.vo.AprovisionamientoResponse;
import com.addcel.televiaservice.servicios.model.vo.BitacoraVO;
import com.addcel.televiaservice.servicios.model.vo.DatosCorreoVO;
import com.addcel.televiaservice.servicios.model.vo.DatosDevolucion;
import com.addcel.televiaservice.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.televiaservice.servicios.model.vo.ReglasResponse;
import com.addcel.televiaservice.servicios.model.vo.ResponseConsultaLCVO;
import com.addcel.televiaservice.servicios.model.vo.ResponsePagoMovilesVO;
import com.addcel.televiaservice.servicios.model.vo.RuleVO;
import com.addcel.televiaservice.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.televiaservice.servicios.model.vo.TBitacoraVO;
import com.addcel.televiaservice.servicios.model.vo.TeleviaDetalle;
import com.addcel.televiaservice.servicios.model.vo.TeleviaInfo;
import com.addcel.televiaservice.servicios.model.vo.UsuarioVO;
import com.addcel.televiaservice.servicios.model.vo.topup_service;
import com.addcel.televiaservice.servicios.utils.AddCelGenericMail;
import com.addcel.televiaservice.servicios.utils.Constantes;
import com.addcel.televiaservice.servicios.utils.ParseTeleviaResponse;
import com.addcel.televiaservice.servicios.utils.Utils;
import com.addcel.televiaservice.servicios.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;
import java.util.Calendar;

@Service
public class PayworksService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PayworksService.class);
	
	@Autowired
	private UtilsService jsonUtils;
	
        @Autowired
	private AntadServiciosMapper mapper;
        
	@Autowired
	private MailSenderService mailSenderService;
	
    @Autowired
    @Qualifier("webServiceAntadBridge")
    private transient WebServiceTemplate webServiceTemplate;
		
	public ModelAndView procesaPago(String json, ModelMap modelo) {
		ModelAndView mav = null;
		MobilePaymentRequestVO pago = null;
		UsuarioVO usuarioVO = null;
		ReglasResponse response = null;
		AbstractVO resValidacion = null;
                LOGGER.info("json contiene : " + json);
                LOGGER.info("modelo contiene : " + modelo);
		try {
			pago = (MobilePaymentRequestVO) jsonUtils.jsonToObject(Utils.decryptJson(json), MobilePaymentRequestVO.class);
			LOGGER.info("PAGO: "+pago.toTrace());
                        LOGGER.info("idUsuario1 : " + pago.getIdUsuario());
    		mav = new ModelAndView("payworks/pagina-3DS");
                        LOGGER.info("idUsuario2 : " + pago.getIdUsuario());
    		usuarioVO = mapper.getUsuario(Long.valueOf(pago.getIdUsuario()));
    		if ("1".equals(usuarioVO.getTipoTarjeta()) || "2".equals(usuarioVO.getTipoTarjeta())){
    			response = validaReglas(usuarioVO.getIdUsuario(), usuarioVO.getNumTarjeta(), 0);
    			if(response == null){
    				resValidacion = validaBloqueos(usuarioVO.getNumTarjeta());
    				if(resValidacion.getIdError() == 0){
    					pago.setNombre(usuarioVO.getNombre() + " " +usuarioVO.getApellidoP()+" "+usuarioVO.getApellidoM());
                		pago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
                		pago.setTarjeta("XXXX XXXX XXXX " + 
                				pago.getTarjeta().substring(pago.getTarjeta().length() - 4 ,pago.getTarjeta().length()));
                		if("1".equals(usuarioVO.getTipoTarjeta())){
                			pago.setTarjetaT("VISA");
                		} else {
                			pago.setTarjetaT("MASTERCARD");
                		}
                                LOGGER.info("Vigencia de la tarjeta1 : " + usuarioVO.getVigencia());
                                LOGGER.info("Vigencia de la tarjeta2 : " + usuarioVO.getVigenciaTarjeta());
                		pago.setVigencia(usuarioVO.getVigenciaTarjeta());
                                pago.setMonto(1);
                		modelo.put("pagoForm", pago);
            			mav.addObject("pagoForm", pago);
    				}
    				
    			} else {
    				mav = new ModelAndView("payworks/pagina-error");
        			mav.addObject("mensajeError", response.getMensaje());
    			}
    		} else if ("3".equals(usuarioVO.getTipoTarjeta())) {
    			mav = new ModelAndView("payworks/pagina-error");
    			mav.addObject("mensajeError", "Disculpe las molestias, continuamos trabajando para habilitar pagos con tarjetas AMEX.");
    		} else {
    			mav = new ModelAndView("payworks/pagina-error");
    			mav.addObject("mensajeError", "Tarjeta no valida");
    		}
		}catch (Exception pe) {
			LOGGER.error("Error General en el proceso de pago.: {}", pe.getMessage());
			pe.printStackTrace();
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Error General en el proceso de pago: " + pe.getMessage());
		}
		return mav;
	}
	
	public ModelAndView procesaPago(MobilePaymentRequestVO pago, ModelMap modelo){
		ModelAndView mav = new ModelAndView("exito");
                LOGGER.debug("dentro de servicio procesaPago" );
		AfiliacionVO afiliacion = null;
		UsuarioVO usuarioVO = null;
		try{
			afiliacion = mapper.buscaAfiliacion("3");
			if(afiliacion == null){
				LOGGER.debug("NO hay afiliacion configurada" );
				mav = new ModelAndView("pagina-error");
				mav.addObject("mensajeError", "No existe una afiliacion configurada.");
			}else{
                                LOGGER.debug("usuario : " + pago.getIdUsuario());
				usuarioVO = mapper.getUsuario(Long.valueOf(pago.getIdUsuario()));
                                LOGGER.debug("despues de recuperar usuario");
				pago.setAfiliacion(afiliacion);
				pago.setNombre(usuarioVO.getNombre() + " " +usuarioVO.getApellidoM());
                                pago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
                                pago.setVigencia(usuarioVO.getVigenciaTarjeta());
                                pago.setTipoTarjeta(Integer.valueOf(usuarioVO.getTipoTarjeta()));
				insertaBitacoras(pago);
				mav = procesaPago3DSecurePayWorks(pago, modelo);
			}
		}catch(Exception e){
			LOGGER.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
			e.printStackTrace();
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Error General en el proceso de pago: " + e.getMessage());
		}
		return mav;
	}
	
	public ModelAndView procesaPago3DSecurePayWorks(MobilePaymentRequestVO pago, ModelMap modelo) {
		ModelAndView mav = null;
		HashMap<String, String> resp = new HashMap<String, String>();
		int respBD = 0;
		double total = 0;
		try{
			LOGGER.info("DATOS ENVIADOS A https://eps.banorte.com/secure3d/Solucion3DSecure.htm : "+pago.toTrace());
			resp.put("ID_AFILIACION", pago.getAfiliacion().getIdAfiliacion());
			resp.put("USUARIO", pago.getAfiliacion().getUsuario());
			resp.put("CLAVE_USR", pago.getAfiliacion().getClaveUsuario());
			resp.put("CMD_TRANS", "VENTA");
			resp.put("ID_TERMINAL", pago.getAfiliacion().getIdTerminal());
			resp.put("Reference3D", pago.getIdTransaccion() + "");
			resp.put("MerchantId", pago.getAfiliacion().getMerchantId());
			resp.put("ForwardPath", pago.getAfiliacion().getForwardPath());
//			resp.put("Expires", pago.getMes() + "/" + pago.getAnio().substring(2, 4));
			resp.put("Expires", pago.getVigencia());
			total = pago.getMonto() + pago.getComision();
			resp.put("Total", Utils.formatoMontoPayworks(total + ""));
			resp.put("Card", pago.getTarjeta());
			resp.put("CardType", pago.getTipoTarjeta() == 1? "VISA": "MC");
                        LOGGER.info("Transaccion : " + pago.getIdTransaccion() + " " + "pago.getCvv2()" + pago.getCvv2());
			respBD = mapper.guardaTBitacoraPIN(pago.getIdTransaccion(), Utils.encryptHard(pago.getCvv2()));
                        LOGGER.info("respBD contiene :" + respBD);
			if(respBD == 1){
				mav = new ModelAndView("payworks/comercio-send");
				mav.addObject("prosa", resp);
				mav.addObject("pagoRequest", pago);
			}
		}catch(Exception e){
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}
	
	public ModelAndView procesaRespuesta3DPayworks(String cadena, ModelMap modelo) {
		ResponsePagoMovilesVO datosResp = null;
		ModelAndView mav = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		String pin = null;
		String msg = null;
		AfiliacionVO afiliacion = null;
		DatosDevolucion devolucion = null;
		try{
			cadena = cadena.replace(" ", "+");
			LOGGER.debug("Cadena procesaRespuesta3DPayworks: " + cadena);
			String[] spl = cadena.split("&");
			String[] data = null;
			for(String cad: spl){
				LOGGER.debug("data:  " + cad);
				data = cad.split("=");
				resp.put(data[0], data.length >= 2? data[1]: "");
			}
			TeleviaInfo pagoInfo = mapper.buscarDetallePago((String) resp.get("Reference3D"));
			if(resp.containsKey("Status") && "200".equals(resp.get("Status"))){
				afiliacion = mapper.buscaAfiliacion("1");
				pin = mapper.buscarTBitacoraPIN((String) resp.get("Reference3D"));
				pin = AddcelCrypto.decryptHard(pin);
				mav=new ModelAndView("payworks/comercioPAYW2");
				mav.addObject("prosa", resp);
				resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
				resp.put("USUARIO", afiliacion.getUsuario());
				resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
				resp.put("CMD_TRANS", "VENTA");
				resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
				resp.put("Total", Utils.formatoMontoPayworks((String)resp.get("Total")));
				resp.put("MODO", "PRD");
				resp.put("NUMERO_CONTROL", resp.get("Reference3D"));
				resp.put("NUMERO_TARJETA", resp.get("Number"));
				resp.put("FECHA_EXP", ((String)resp.get("Expires")).replaceAll("%2F", ""));
				resp.put("CODIGO_SEGURIDAD", pin);
				resp.put("MODO_ENTRADA", "MANUAL");
				resp.put("URL_RESPUESTA", Constantes.VAR_PAYW_URL_BACK_W2);
				resp.put("IDIOMA_RESPUESTA", "ES");
				devolucion = new DatosDevolucion();
				if(resp.get("XID") != null){
					resp.put("XID", ((String)resp.get("XID")).replaceAll("%3D", "="));
					resp.put("CAVV", ((String)resp.get("CAVV")).replaceAll("%3D", "="));
					devolucion.setXid((String)resp.get("XID"));
					devolucion.setCavv((String)resp.get("CAVV"));
				}
				resp.put("CODIGO_SEGURIDAD", pin);
				devolucion = new DatosDevolucion();
				devolucion.setIdTransaccion(pagoInfo.getIdTransaccion());
				devolucion.setXid("");
				devolucion.setCavv("");
				devolucion.setEci((String)resp.get("ECI"));
//				mapper.insertaDatosDevolucion(devolucion);
				LOGGER.info("DATOS ENVIADO A PAYWORK2: "+resp.toString());
			} else {
				mav=new ModelAndView("payworks/pagina-error");
				datosResp = new ResponsePagoMovilesVO();
				msg = "PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": "MASTER") + " 3D RECHAZADA: " + resp.get("Status") ;
				datosResp.setIdError(Integer.parseInt((String)resp.get("Status")));
				datosResp.setMensajeError("El pago fue rechazado. " + 
						(resp.get("Status") != null? "Clave: " + resp.get("Status"): "") + ", Descripcion: " + Constantes.errorPayW.get(resp.get("Status")) );
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
				updateBitacoras(pagoInfo, datosResp, null, null, null, null, null, null, msg);
				mav.addObject("mensajeError", datosResp.getMensajeError());
			}
		}catch(Exception e){
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			mav=new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}
	
	public ModelAndView payworks2Respuesta(String NUMERO_CONTROL,
			String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW,
			String FECHA_REQ_CTE, String CODIGO_AUT,
			String CODIGO_PAYW, String RESULTADO_AUT, String BANCO_EMISOR,
			String ID_AFILIACION, String TIPO_TARJETA, String MARCA_TARJETA,
			ModelMap modelo, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("exito");
		ResponsePagoMovilesVO datosResp = null;
		ResponseConsultaLCVO respServ = new ResponseConsultaLCVO();
		DatosCorreoVO datosCorreoVO=null;
		String msg = null;
		AprovisionamientoResponse respuesta = null; //cambiar jcarlos
                topup_service respuestaaux = null;
                String json = null;
		try{
			LOGGER.info("RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL+"REFERENCIA: " + REFERENCIA
					+"FECHA_RSP_CTE: " + FECHA_RSP_CTE+"TEXTO: " + TEXTO+"RESULTADO_AUT: " + RESULTADO_AUT
					+"RESULTADO_PAYW: " + RESULTADO_PAYW+"FECHA_REQ_CTE: " + FECHA_REQ_CTE+"CODIGO_PAYW: " + CODIGO_PAYW
					+"CODIGO_PAYW: " + CODIGO_PAYW+"BANCO_EMISOR: " + BANCO_EMISOR+"TIPO_TARJETA: " + TIPO_TARJETA
					+"MARCA_TARJETA: " + MARCA_TARJETA);			
			if(MARCA_TARJETA != null){
				MARCA_TARJETA = MARCA_TARJETA.replaceAll("-", "");
				TIPO_TARJETA = MARCA_TARJETA + "/" + TIPO_TARJETA;
			}
			//Comentar datos cuando en PROD, cuando paywork regrese informacion
			if (StringUtils.equals(Constantes.MODO, Constantes.AUT)) {
				BANCO_EMISOR = "BANORTE";
				TIPO_TARJETA = "VISA/CREDITO";
			}
			TEXTO = Utils.cambioAcento(TEXTO);
			
			TeleviaInfo pagoInfo = mapper.buscarDetallePago(NUMERO_CONTROL);
			LOGGER.info("Consulta detalle pago: "+pagoInfo.toTrace());
			datosResp = new ResponsePagoMovilesVO();
			datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
			datosResp.setReferencia(pagoInfo.getReferencia());
			datosResp.setMonto(pagoInfo.getMonto());
			pagoInfo.setTarjeta(AddcelCrypto.decryptTarjeta(pagoInfo.getTarjeta()));
			datosResp.setTarjeta(pagoInfo.getTarjeta().substring(0, 6) + " XXXXXX " + pagoInfo.getTarjeta().substring(pagoInfo.getTarjeta().length() - 4));
			datosResp.setBancoTarjeta(BANCO_EMISOR);
			
			if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				msg = "EXITO PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": pagoInfo.getTipoTarjeta() == 2? "MASTER": "VISA") 
						+ " "+BANCO_EMISOR+", "+TIPO_TARJETA;
				datosResp.setAutorizacion(CODIGO_AUT);
	//			mapper.borrarTBitacoraPIN(NUMERO_CONTROL);
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setUsuario(String.valueOf(pagoInfo.getIdUsuario()));
				datosResp.setFecha(respServ.getFecha());
				datosResp.setHora(respServ.getHora());
				datosResp.setReferen(pagoInfo.getReferencia());
				datosResp.setMedioPresentacion("Internet");
				datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
				datosResp.setTipo(TIPO_TARJETA);
				mav = new ModelAndView("payworks/exito");
				datosCorreoVO = new DatosCorreoVO();
				datosCorreoVO.setEmail(pagoInfo.geteMail());
                                json = jsonUtils.objectToJson(pagoInfo);
                                respuesta = (AprovisionamientoResponse) jsonUtils.jsonToObject(json, AprovisionamientoResponse.class);
				//respuesta = notificacionComercio(pagoInfo);// cambiar jcarlos
                                respuestaaux = aprovisionaTelevia(pagoInfo);
				if(respuestaaux.getRet_code().equals("0")){
                                        
					double total = Double.parseDouble(respuestaaux.getAmount()) + pagoInfo.getComision();
					try {
						enviaCorreo(respuesta, total, CODIGO_AUT, String.valueOf(pagoInfo.getIdTransaccion()), pagoInfo.getNombre(), pagoInfo.getConcepto());
					} catch (Exception e) {
						LOGGER.error("No se pudo enviar el correo..");
					}
					datosResp.setIdError(0);
					updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, 
								msg+" "+pagoInfo.getConcepto());
					mav.addObject("mensajeError", "<BR>Estimado usuario, su pago ha sido exitoso." 
							+"<br><BR><BR> Pago: "+pagoInfo.getConcepto()
							+"<br>Fecha: "+FECHA_REQ_CTE
							+"<br>Referencia: "+REFERENCIA
							+"<br>Folio: "+RESULTADO_PAYW
							+"<br>Sucursal: 1"
							+"<br>Caja: 1"
							+"<br>Forma de pago: Tarjeta de credito"
							+"<br>Numero de Autorizacion: "+CODIGO_AUT
							+"<br>Folio XCD: "+respuesta.getNumAuth()
							+"<br>Importe: "+respuesta.getMonto()
							+"<br>Comision: "+respuesta.getComision()
							+"<br>Total (incluida comision): "+total
							+"<BR><BR>"+respuesta.getMensajeTicket());
				} else {
					datosResp.setConcepto("Error al aprovisionar TELEVIA: "+respuesta.getMensajeCajero());
					datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
					datosResp.setIdError(respuesta.getIdError());
					datosResp.setMensajeError(respuesta.getMensajeAntad());
					updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, datosResp.getMensajeError());
					mav = new ModelAndView("payworks/pagina-error");
					mav.addObject("mensajeError", datosResp.getMensajeError());
//					mav = enviaDevolucion(pagoInfo, modelo, REFERENCIA);
				}
			} else {
//				mapper.borrarTBitacoraPIN(NUMERO_CONTROL);
				msg = "PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": "MASTER") + " 3D " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADA": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADA":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida")
						+ ": " + TEXTO ;
				
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setIdError(CODIGO_PAYW != null? CODIGO_PAYW.startsWith("PAYW-")? Integer.parseInt(CODIGO_PAYW.replace("PAYW-", "")): Integer.parseInt(CODIGO_PAYW): RESULTADO_AUT != null? Integer.parseInt(RESULTADO_AUT): 0);
				datosResp.setMensajeError("El pago fue " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADO": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADO":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida") +BANCO_EMISOR+", "+TIPO_TARJETA+
					" . " + (CODIGO_PAYW != null? "Clave: " +CODIGO_PAYW:RESULTADO_AUT != null? RESULTADO_AUT: "") + ", Descripcion: " + TEXTO  );
				
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("mensajeError", datosResp.getMensajeError());
				updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, msg);
			}
		}catch(Exception e){
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", 
					"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
					+ "Por favor vuelva a intentarlo en unos minutos.");
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			datosResp = new ResponsePagoMovilesVO(100, 
					"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
							+ "Por favor vuelva a intentarlo en unos minutos.");
		}finally{
			modelo.put("datosResp", datosResp);
			mav.addObject("datosResp", datosResp);
		}
		return mav;	
	}
	
        public topup_service aprovisionaTelevia(TeleviaInfo transaccion){
		topup_service response = new topup_service();
		String iduser, idpass, resp = null;
                long idorder = 0L;
		try {
			Calendar mC = Calendar.getInstance();
			String wfecha = agregaCeros(new StringBuilder().append("").append(mC.get(5)).toString(), 2) + "/"
					+ agregaCeros(new StringBuilder().append("").append(mC.get(2) + 1).toString(), 2) + "/" + mC.get(1) + " "
					+ agregaCeros(new StringBuilder().append("").append(mC.get(11)).toString(), 2) + ":"
					+ agregaCeros(new StringBuilder().append("").append(mC.get(12)).toString(), 2) + ":"
					+ agregaCeros(new StringBuilder().append("").append(mC.get(13)).toString(), 2);
			String prefijo = "";
			String puntos = "";
			if (transaccion.getReferencia().length() != 11) {
				while (transaccion.getReferencia().length() < 8) {
					transaccion.setReferencia("0" + transaccion.getReferencia());
				}
				if ((transaccion.getReferencia().length() == 8) && (Long.parseLong(transaccion.getReferencia()) >= 20000000L)) {
					prefijo = "IMDM";
				}
				if ((transaccion.getReferencia().length() == 8) && (Long.parseLong(transaccion.getReferencia()) < 20000000L)) {
					prefijo = "CPFI";
				}
				if (transaccion.getReferencia().length() == 8) {
					puntos = "..";
				}
			}
                        
                    iduser = mapper.getParametro("@ID_USER_TELEVIA");
                    idpass = mapper.getParametro("@ID_PASS_TELEVIA");
	            idorder = mapper.getMaxIdOrder();
                    
                    LOGGER.info("Numero de tag TELEVIA  : " + transaccion.getReferencia());
                    
			StringBuffer pXML = new StringBuffer().append("<topup_service><request>").append("<user>").append(iduser)
				.append("</user>").append("<password>").append(idpass).append("</password>")
				.append("<id_order>").append(idorder+1).append("</id_order>")
				.append("<id_tag>").append(transaccion.getReferencia()).append("</id_tag>").append("<amount>")
				.append(transaccion.getCargo()).append("</amount>").append("</request></topup_service>");

                        System.out.println("MPX COMPRA TELEVIA : " + pXML);
                        com.addcel.televiaservice.servicios.client.televia.TopupServiceSoapBindingStub binding = null;
                    try {
                        binding = (com.addcel.televiaservice.servicios.client.televia.TopupServiceSoapBindingStub) new com.addcel.televiaservice.servicios.client.televia.TopupServiceServiceLocator().getTopupService();
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
			com.addcel.televiaservice.servicios.client.televia.ObjectFactory factory = new com.addcel.televiaservice.servicios.client.televia.ObjectFactory();
			Reload reload = factory.createReload();
			reload.setSXML(pXML.toString());
			//resp = binding.doTopupService(pXML.toString());  //produccion}
                        resp = "<topup_service><request><user>mobilecardmx</user><password>m0c4Rmx01pwd</password><id_order>100000002</id_order><id_tag>09900032558</id_tag><amount>1</amount></request><response><user>smsservice</user><password>smsservicepwd</password><balance>8</balance><seq>15575948</seq><ret_code>0</ret_code><ret_msg/></response></topup_service>";
                        //codigo hardcodeado quitar cuando se ponga en produccion 
                        String raux = Utils.transformarXMLRes(resp);
			ParseTeleviaResponse parseTelevia = new ParseTeleviaResponse();
			response = parseTelevia.parseResponseXStream(raux);
			LOGGER.info("RESPUESTA DE TELEVIA: ID TRANSACCION: "+transaccion.getIdTransaccion()
					+" AMOUNT: "+response.getAmount()+" TRX_NO: "+response.getId_order()+" AUTONO: "+response.getSeq()
                                        +" AMOUNT: "+response.getAmount()+" TRX_NO: "+response.getId_order()+" AUTONO: "+response.getBalance()
					+" RESPONSE CODE: "+response.getRet_code()+" DESCRIPCION CODE: "+response.getRet_msg());
		} catch (Exception e) {
			LOGGER.info("ERROR AL APROVISIONAR LA TRANSACCION TELEVIA: "+transaccion.getIdTransaccion()+" CAUSA: "+e.getMessage());
			LOGGER.error("ERROR: "+e.getLocalizedMessage());
			response.setRet_code("-01");
			response.setRet_msg("Error al comunicarse con TELEVIA");
		}
		return response;
	}
        
        private String agregaCeros(String cad, int NumCeros) {
		String tmp = "00000000000000000000000000000";
		return (tmp + cad).substring((tmp + cad).length() - NumCeros, (tmp + cad).length());
	}
        
	public ModelAndView enviaDevolucion(AntadInfo pagoInfo, ModelMap modelo, String referenciaPW) {
		ModelAndView mav = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		String pin = null;
		AfiliacionVO afiliacion = null;
		DatosDevolucion devolucion = null;
		try{				
			afiliacion = mapper.buscaAfiliacion("1");
			devolucion = mapper.getDatosDevolucion(pagoInfo.getIdTransaccion());
			pin = mapper.buscarTBitacoraPIN(String.valueOf(pagoInfo.getIdTransaccion()));
			pin = AddcelCrypto.decryptHard(pin);
			mav=new ModelAndView("payworks/comercioPAYWDevo");
			resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
			resp.put("USUARIO", afiliacion.getUsuario());
			resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
			resp.put("CMD_TRANS", "DEVOLUCION");
			resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
			resp.put("Total", Utils.formatoMontoPayworks(pagoInfo.getMonto()+""));
			resp.put("MODO", "PRD");
			resp.put("REFERENCIA", referenciaPW);
			resp.put("MODO_ENTRADA", "MANUAL");
			resp.put("URL_RESPUESTA", Constantes.VAR_PAYW_URL_BACK_DEV);
			resp.put("IDIOMA_RESPUESTA", "ES");
			resp.put("XID", devolucion.getXid());
			resp.put("CAVV", devolucion.getCavv());
			resp.put("ECI", devolucion.getEci());
			resp.put("ESTATUS_3D", "200");
			mav.addObject("prosa", resp);

		}catch(Exception e){
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			mav=new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}
	
	public ModelAndView payworks2DevoRespuesta(/**String NUMERO_CONTROL, **/
			String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW,
			String FECHA_REQ_CTE, String CODIGO_AUT,
			String ID_AFILIACION, 
			ModelMap modelo, HttpServletRequest request) {
		
		ModelAndView mav = new ModelAndView("payworks/exito");
		ResponsePagoMovilesVO datosResp = null;
//		ResponseConsultaLCVO respServ = new ResponseConsultaLCVO();
		String msg = null;
		String NUMERO_CONTROL = null;
		try{
			if(NUMERO_CONTROL == null){
				NUMERO_CONTROL = "156124";
			}
			LOGGER.debug("DEVO NUMERO_CONTROL: " + NUMERO_CONTROL);
			LOGGER.debug("DEVO REFERENCIA: " + REFERENCIA);
			LOGGER.debug("DEVO FECHA_RSP_CTE: " + FECHA_RSP_CTE);
			LOGGER.debug("DEVO TEXTO: " + TEXTO);
			LOGGER.debug("DEVO RESULTADO_PAYW: " + RESULTADO_PAYW);
			LOGGER.debug("DEVO FECHA_REQ_CTE: " + FECHA_REQ_CTE);
			LOGGER.debug("DEVO CODIGO_AUT: " + CODIGO_AUT);
			LOGGER.debug("DEVO ID_AFILIACION: " + ID_AFILIACION);
			TEXTO = Utils.cambioAcento(TEXTO);
			TeleviaInfo pagoInfo = mapper.buscarDetallePago(NUMERO_CONTROL);
			LOGGER.info("Consulta detalle pago: "+pagoInfo.toTrace());
			datosResp = new ResponsePagoMovilesVO();
			try{
				NUMERO_CONTROL = NUMERO_CONTROL.replaceAll("-D", ""); 
			}catch(Exception e){
				LOGGER.error("Error en parseo NUMERO_CONTROL: " + e.getMessage());
			}
			datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
			datosResp.setReferencia(pagoInfo.getReferencia());
			datosResp.setMonto(pagoInfo.getMonto());
			datosResp.setTarjeta(pagoInfo.getTarjeta().substring(0, 6) + " XXXXXX " + pagoInfo.getTarjeta().substring(pagoInfo.getTarjeta().length() - 4));
			if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				msg = "DEVOLUCION EXITO PAGO - RECHAZADA POR EL PROVEEDOR";
				datosResp.setAutorizacion(CODIGO_AUT);
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setMedioPresentacion("Internet");
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("mensajeError", "<BR>Estimado usuario, detectamos un error al aprovisionar su servicio."
						+"<BR><BR><BR> Se procedio a realizar la devolucion de su pago. " 
						+"<br><BR><BR> Estado de la devolucion: "+msg
						+"<BR><BR>Clave: " +RESULTADO_PAYW 
						+"<BR><BR>Autorizacion del reverso: "+CODIGO_AUT);
				
			}else{
				msg = "DEVOLUCION " +
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADA": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADA":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida")
						+ ": " + TEXTO ;
				
				datosResp.setFechaTran(new SimpleDateFormat(Constantes.FORMATO_FECHA_COMPRA).format(new Date()));
				datosResp.setIdError(-1);
				datosResp.setMensajeError("Devolucion: " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADO": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADO":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida") +
					".<BR> " +
					"Descripcion: " + TEXTO  );
				
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("mensajeError", "<BR><BR>   " + datosResp.getMensajeError());
			}
			datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
			updateBitacoras(pagoInfo, datosResp, null, null, REFERENCIA, CODIGO_AUT, null, null, msg);
		}catch(Exception e){
			mav=new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", 
					"Error General al realizar el pago : " + (e.getMessage()!= null && e.getMessage().length() >100? 
							e.getMessage().substring(0,100): e.getMessage()));
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			datosResp = new ResponsePagoMovilesVO(100, "Error General al realizar el pago : " + (e.getMessage()!= null && e.getMessage().length() >100? e.getMessage().substring(0,100): e.getMessage()));
			LOGGER.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
		}finally{
			modelo.put("datosResp", datosResp);
			mav.addObject("datosResp", datosResp);
		}
		return mav;	
	}
	
	private AprovisionamientoResponse notificacionComercio(AntadInfo pagoInfo) {
		ProcesaAutorizacion autorizacion = null;
		ProcesaAutorizacionResponse response = null;
		ObjectFactory factory = new ObjectFactory();
		AprovisionamientoResponse respuesta = null;
		String json = null;
		try {
			json = jsonUtils.objectToJson(pagoInfo);
			json = Utils.encryptJson(json);
			autorizacion = new ProcesaAutorizacion();
			autorizacion.setJson(json);
			response = (ProcesaAutorizacionResponse) webServiceTemplate.marshalSendAndReceive(
					factory.createProcesaAutorizacion(autorizacion));
			json = Utils.decryptJson(response.getReturn());
			respuesta = (AprovisionamientoResponse) jsonUtils.jsonToObject(json, AprovisionamientoResponse.class);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_PROCESA_PAGO+" ["+e.getCause()+"]");
			
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_PROCESA_PAGO+json);
		}
		return respuesta;
	}

	private void insertaBitacoras(MobilePaymentRequestVO pago) throws Exception{
		BitacoraVO tb = null;
		try {
			pago.setTarjeta(pago.getTarjeta()!= null  && !"".equals(pago.getTarjeta())? AddcelCrypto.encryptTarjeta(pago.getTarjeta()): "");
			tb = new BitacoraVO(Long.valueOf(pago.getIdUsuario()), ""+pago.getConcepto() + " "+( pago.getTipoTarjeta() == 3?"AMEX":"VISA"), 
					(pago.getSoftware() != null? pago.getSoftware() + " " + pago.getModelo(): "Pago "), 
					"Referencia: " + pago.getReferencia() + ", Importe: " + pago.getMonto(), 
					pago.getTarjeta(), pago.getTipo(), 
					pago.getSoftware(), pago.getModelo(), pago.getImei(), 
					pago.getMonto() );
			tb.setIdProveedor(6);
			mapper.insertaBitacoraTransaccion(tb);
			pago.setIdTransaccion(tb.getIdBitacora());

			/*TBitacoraProsaVO tbProsa = new TBitacoraProsaVO(tb.getIdBitacora(),
					pago.getIdUsuario(),null, null, null, 
					"" +pago.getConcepto() + " "+ (pago.getTipoTarjeta() ==3?"AMEX":"VISA"),
					String.valueOf(pago.getMonto()), pago.getComision()+"", "0", "0");	
			tbProsa.setTarjeta("");
			tbProsa.setTransaccion("");
			mapper.addBitacoraProsa(tbProsa);*/
			TeleviaDetalle televiaDetalle = new TeleviaDetalle();
                        televiaDetalle.setIdTransaccion(pago.getIdTransaccion());
			televiaDetalle.setDescripcion(pago.getConcepto());
			televiaDetalle.setTipoTarjeta(pago.getTipoTarjeta());
			televiaDetalle.setTotal(pago.getMonto());
			televiaDetalle.setComision(pago.getComision());
			televiaDetalle.setReferencia(pago.getReferencia());
			televiaDetalle.setEmisor(String.valueOf(pago.getEmisor()));
			televiaDetalle.setCodigoRespuesta("");
                        LOGGER.info("TeleviaDetalle: "+televiaDetalle.toString());
			mapper.insertaTeleviaDetalle(televiaDetalle);		
			LOGGER.info("DETALLE TELEVIA GUARDADO EXITOSAMENTE - ID TRANSACCION: "+pago.getIdTransaccion()+", LOGIN: "+pago.getUsuario());
			if(pago.getTarjeta() != null && !"".equals(pago.getTarjeta())){
				pago.setTarjeta(AddcelCrypto.decryptTarjeta(pago.getTarjeta()));
			}
		} catch (Exception e) {
			LOGGER.error("Error al guardar en bitacoras,  id_bitacora: {}", tb.getIdBitacora(), e);
			throw new Exception(e);
		}
	}
	
	private void updateBitacoras(TeleviaInfo pago, ResponsePagoMovilesVO datosResp,  String REFERENCIA, String CODIGO_AUT, 
			String REFERENCIA_CAN, String CODIGO_AUT_CAN, 
			String BANCO_EMISOR, String TIPO_TARJETA, String msg){
		TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
		TBitacoraVO tbitacoraVO = new TBitacoraVO();
		try{
        	LOGGER.debug("Inicio Update TBitacora.");
        	tbitacoraVO.setIdBitacora(pago.getIdTransaccion());
        	tbitacoraVO.setBitConcepto(msg);
        	tbitacoraVO.setBitTicket(msg);
        	tbitacoraVO.setBitNoAutorizacion(datosResp.getAutorizacion());
        	if(datosResp.getIdError() == 0){
        		tbitacoraVO.setBitStatus(1);
        	} else {
        		tbitacoraVO.setBitStatus(datosResp.getIdError());
        	}
        	tbitacoraVO.setBitCodigoError(datosResp.getIdError() != 0? datosResp.getIdError(): 0);
        	tbitacoraVO.setDestino("Referencia: " + pago.getReferencia() + "-" + TIPO_TARJETA);
        	mapper.updateBitacora(tbitacoraVO);
        	LOGGER.debug("Fin Update TBitacora.");
        	LOGGER.debug("Inicio Update TBitacoraProsa.");
        	tbitacoraProsaVO.setIdBitacora(pago.getIdTransaccion());
        	tbitacoraProsaVO.setConcepto(msg);
        	tbitacoraProsaVO.setAutorizacion(datosResp.getAutorizacion());
        	//mapper.updateBitacoraProsa(tbitacoraProsaVO);
        	//LOGGER.debug("Fin Update TBitacoraProsa.");
        } catch(Exception e){
        	LOGGER.error("Error al actualizar las bitacoras: ", e);
        }
	}
	
	/*private void enviaCorreo(AprovisionamientoResponse response, double total,
			String eM_Auth, String eM_OrderID, String nombre, String concepto)*/
        private void enviaCorreo(AprovisionamientoResponse response, double total,
			String eM_Auth, String eM_OrderID, String nombre, String concepto) {
		DatosCorreoVO datosCorreoVO = new DatosCorreoVO();
		String ticket = null;
		String bodyMail = null;
		String host = null;
	    int port = 0;
	    String username = null;
	    String password = null;
	    String from = null; 
	    Properties props = null;
		try {
			datosCorreoVO.setDescServicio(response.getConcepto());
			datosCorreoVO.setFecha(UtilsService.getFechaActual());
			datosCorreoVO.setReferenciaServicio(response.getReferencia());
			datosCorreoVO.setIdBitacora(Long.valueOf(eM_OrderID));
			datosCorreoVO.setNoAutorizacion(eM_Auth);
			datosCorreoVO.setFolioXcd(response.getNumAuth());
			datosCorreoVO.setImporte(response.getMonto());
			datosCorreoVO.setComision(response.getComision());
			datosCorreoVO.setMonto(total);
			datosCorreoVO.setNombre(nombre);
			datosCorreoVO.setConcepto(concepto);
			ticket = response.getMensajeTicket().replace("|", "<br>");
			ticket = ticket.replace("01(81)-8988-0200", "01–800–925–5001");
			datosCorreoVO.setTicket(ticket);
			datosCorreoVO.setEmail(response.geteMail());
			bodyMail = mapper.getParametro("@MENSAJE_COMPRAANTAD");
			host =  mapper.getParametro("@SMTP"); 
            port = Integer.parseInt( mapper.getParametro("@SMTP_PORT") ); 
            username =  mapper.getParametro("@SMTP_USER"); 
            password =  mapper.getParametro("@SMTP_PWD"); 
            from = mapper.getParametro("@SMTP_MAIL_SEND");
            props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            LOGGER.debug("HOST: " + host);
            LOGGER.debug("PORT: " + port);
            LOGGER.debug("USERNAME: " + username);
//            AddCelGenericMail.generatedMailCommons(datosCorreoVO, bodyMail, props, from, host, port, username, password);
			AddCelGenericMail.generatedMail(datosCorreoVO, bodyMail);
		} catch (Exception e) {
			LOGGER.error("Error al mandar el correo: "+e.getCause());
		}	
	}

	private ReglasResponse validaReglas(long idUsuario, String tarjeta, int idProducto) {
		ReglasResponse response = null;
		String numUsuario = null;
		String numTarjeta = null;
		RuleVO valoresU = null;
		RuleVO valoresT = null;
		try {
			numUsuario = mapper.getParametro("@ANTAD_NUM_TRAN_USUARIO");
			numTarjeta = mapper.getParametro("@ANTAD_NUM_TRAN_TARJETA");

			valoresU = mapper.selectRule(idUsuario, tarjeta, "45", "@ANTAD_NUM_TRAN_USUARIO");
			valoresT = mapper.selectRule(idUsuario, tarjeta, "45", "@ANTAD_NUM_TRAN_TARJETA");
			LOGGER.info("RESPUESTA DE REGLAS DE VALIDACION: TRANSACCIONES USUARIO: "+numUsuario
					+", TRANSACCION TARJETA: "+numTarjeta+", BITACORA USUARIO: "+valoresU.getNumero()
					+", BITACORA TARJETA: "+valoresT.getNumero());
			
			if (((Integer) valoresU.getNumero()).doubleValue() >= Integer.valueOf(numUsuario)) {
				response = new ReglasResponse(0, "920",
						"En el periodo " + valoresU.getFechaInicio() + " a " + valoresU.getFechaFin()
								+ " a superado el número maximo de transacciones permitidas: " + numUsuario);
			} else if (((Integer) valoresT.getNumero()).doubleValue() >= Integer.valueOf(numTarjeta)) {
				response = new ReglasResponse(0, "922",
						"En el periodo " + valoresT.getFechaInicio() + " a " + valoresT.getFechaFin()
								+ " a superado el número maximo de transacciones permitidas para una Tarjeta: "
								+ numTarjeta);
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error al validar Reglas de Negocio.", e);
			response = new ReglasResponse(0, "920", "Ocurrio un error al validar Reglas de Negocio.");
		}
		return response;
	}
	
	private AbstractVO validaBloqueos(String tarjeta) {
		AbstractVO response = new AbstractVO();
		int bloqueoTarjeta = 0;
		bloqueoTarjeta = mapper.getBloqueoTarjeta(tarjeta);
		if(bloqueoTarjeta != 0){
			response.setIdError(-1);
			response.setMensajeError("Estimado usuario, hemos detectado actividad sospechosa por lo cual su tarjeta se encuentra bloqueada.");
		} 
		return response;
	}
	
}

package com.addcel.televiaservice.servicios.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.addcel.televiaservice.servicios.client.televia.ObjectFactory;
import com.addcel.televiaservice.servicios.client.televia.Reload;
import com.addcel.televiaservice.servicios.client.televia.*;
import com.addcel.televiaservice.servicios.model.mapper.TeleviaMapper;
import com.addcel.televiaservice.servicios.model.vo.AbstractVO;
import com.addcel.televiaservice.servicios.model.vo.AfiliacionVO;
import com.addcel.televiaservice.servicios.model.vo.DatosCorreoVO;
import com.addcel.televiaservice.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.televiaservice.servicios.model.vo.TeleviaVO;
import com.addcel.televiaservice.servicios.model.vo.ProcomVO;
import com.addcel.televiaservice.servicios.model.vo.Producto;
import com.addcel.televiaservice.servicios.model.vo.ReglasResponse;
import com.addcel.televiaservice.servicios.model.vo.topup_service;
import com.addcel.televiaservice.servicios.model.vo.RespuestaServicioVO;
import com.addcel.televiaservice.servicios.model.vo.RuleVO;
import com.addcel.televiaservice.servicios.model.vo.TBitTeleviaDet;
import com.addcel.televiaservice.servicios.model.vo.TBitacoraVO;
import com.addcel.televiaservice.servicios.model.vo.UsuarioVO;
import com.addcel.televiaservice.servicios.utils.AddCelGenericMail;
import com.addcel.televiaservice.servicios.utils.Constantes;
import com.addcel.televiaservice.servicios.utils.ParseTeleviaResponse;
import com.addcel.televiaservice.servicios.utils.Utils;
import com.addcel.televiaservice.servicios.utils.UtilsService;
import com.addcel.televiaservice.servicios.service.PayworksService;

import com.addcel.utils.AddcelCrypto;

import crypto.Crypto;
import org.apache.axis.AxisProperties;
import org.springframework.ui.ModelMap;
import com.google.gson.Gson;

@Service
public class TELEVIAService {
   
	private static final Logger LOGGER = LoggerFactory.getLogger(TELEVIAService.class);
	
	private static final String TELEFONO_SERVICIO = "5525963513";
	
	private static DecimalFormat DF;
	
        @Autowired
	private PayworksService parworksService;
        
	@Autowired
	private UtilsService jsonUtils;
	
	@Autowired
	private TeleviaMapper mapper;
	
	public ModelAndView pagoTelevia3DS(String json) {
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		ProcomVO procom = null;
		RespuestaServicioVO respuesta = null;
		TeleviaVO compra = null;
		AbstractVO resValidacion = null;
		ReglasResponse response = null;
		Producto producto = null;
		AfiliacionVO afiliacion = null;
		double comision = 0;
		double total = 0;
                String data = "";
                MobilePaymentRequestVO mprvo = new MobilePaymentRequestVO();
		try {
			json = AddcelCrypto.decryptSensitive(json);
			compra = (TeleviaVO) jsonUtils.jsonToObject(json, TeleviaVO.class);
			LOGGER.info("INICIANDO PAGO TELEVIA 3D SECURE: ID USUARIO - {}", compra.getLogin());
			respuesta = new RespuestaServicioVO();
			usuario = mapper.validaUsuario(compra.getLogin());
                        mprvo.setIdUsuario(Long.toString(usuario.getIdUsuario()));
                        mprvo.setCvv2(compra.getCvv2());
                        mprvo.setTarjeta(compra.getTarjeta());
                        mprvo.setImei(compra.getImei());
                        mprvo.setModelo(compra.getModelo());
                        mprvo.setSoftware(compra.getSoftware());
                        mprvo.setTipo(compra.getTipo());
                        data = jsonUtils.objectToJson(mprvo);
                        LOGGER.info("idusuario : " + usuario.getIdUsuario());
                        ModelMap modelo = new ModelMap();
			if(usuario != null){
				if ("1".equals(usuario.getTipoTarjeta()) || "2".equals(usuario.getTipoTarjeta())){
					LOGGER.debug("TELEVIA validacion Reglas negocio, idUsuario: " + usuario.getIdUsuario()+" "+ setSMS(compra.getTarjeta()));
					response = validaReglas(usuario.getIdUsuario(), setSMS(compra.getTarjeta()), 0);
					if(response == null){
						producto = mapper.getProductoByClaveWS(compra.getProducto());
						comision = mapper.getComisionXProducto(Integer.parseInt(producto.getProveedor()));
						compra.setComision(Double.valueOf(comision));
						insertaBitacoras(compra, usuario, producto);
						resValidacion = validaBloqueos(compra.getTarjeta(), compra.getImei());
						if(resValidacion.getIdError() == 0){
							total = Double.valueOf(producto.getMonto())+comision;
							afiliacion = mapper.buscaAfiliacion("3");
							procom = new ProcomService().comercioFin(compra.getIdBitacora(), 
									total, afiliacion);
							/*return procesaRespuestaProsa("approved", "100", ""+compra.getIdBitacora(), 
									"7627488", "A", "A", "000000067538", "766220", "A");*/
                                                          return parworksService.procesaPago(Utils.encryptJson(data), modelo);
						} else {
							json =  jsonUtils.objectToJson(resValidacion);
						}
					} else {
						json =  jsonUtils.objectToJson(response);
					}
					
				} else if ("3".equals(usuario.getTipoTarjeta())) {
					respuesta.setIdError(-1);
					respuesta.setMsgError("Disculpe las molestias, continuamos trabajando para habilitar pagos con tarjetas AMEX.");
					LOGGER.error("Error Intento de pago AMEX.");
				} else {
					respuesta.setIdError(-1);
					respuesta.setMsgError("Tipo de Tarjeta no valida.");
					LOGGER.error("Error tipo de tarjeta registrada: {}" + usuario.getTipoTarjeta());
				}
			} else {
				respuesta.setIdError(-100);
				respuesta.setMsgError("Usuario no encontrado");
			}
		} catch (Exception e) {
			respuesta.setIdError(-100);
			respuesta.setMsgError("Ocurrio un error al realizar el pago del servicio, problemas internos -3");
			LOGGER.error("Error al realizar la recarga", e);
		} finally {
			if (respuesta.getIdError() == 0) {
				LOGGER.info("ENVIADO INFORMACION A 3DSECURE - ID TRANSACCION: "+compra.getIdBitacora());
				mav = new ModelAndView("3ds/comerciofin");
				mav.addObject("prosa", procom);
			} else {
				mav = new ModelAndView("3ds/error");
				mav.addObject("descError", respuesta.getMsgError());
			}
		}
		return mav;
	}
	
	public ModelAndView procesaRespuestaProsa(String eM_Response,
			String eM_Total, String eM_OrderID, String eM_Merchant,
			String eM_Store, String eM_Term, String eM_RefNum, String eM_Auth,
			String eM_Digest) {
		ModelAndView mav = null;
		RespuestaServicioVO respuesta = null;
		TBitacoraVO transaccion = null;
		topup_service response = null;
                TBitTeleviaDet res = new TBitTeleviaDet();
		try {
			LOGGER.debug("RESPUESTA 3DSECURE PARA LA TRANSACCION - " + eM_OrderID +" EM_Total: " + eM_Total 
					+" EM_Merchant: " + eM_Merchant +" EM_RefNum: " + eM_RefNum + " EM_Auth: " + eM_Auth
					+" eM_Response: "+eM_Response+" eM_Store: "+eM_Store+" eM_Term: "+eM_Term
					+" eM_Digest: "+eM_Digest);
			respuesta = new RespuestaServicioVO();
			transaccion = mapper.getDatosTransaccion(Long.valueOf(eM_OrderID));
			transaccion.setIdBitacora(Long.valueOf(eM_OrderID));
			if (!eM_Auth.equals("000000") || eM_Auth.equals("A")) {
				LOGGER.debug("TRANSACCION - " + eM_OrderID+" Aprobada - AUTORIZACION - "+eM_Auth);
				double total = 0;
				String decimales = eM_Total.substring(eM_Total.length() -2, eM_Total.length());
				String enteros = eM_Total.substring(0, eM_Total.length() -2);
				enteros = enteros+"."+decimales;
				total = Double.valueOf(enteros);
				total = transaccion.getCargo() + transaccion.getComision();
				response = aprovisionaTelevia(transaccion);
                                res.setId_bitacora(Integer.parseInt(eM_OrderID));
                                res.setId_order(Long.parseLong(response.getId_order()));
                                res.setId_tag(Long.parseLong(response.getId_tag()));
                                res.setImporte(response.getAmount());
                                res.setSeq(Long.parseLong(response.getSeq()));
                                res.setRet_code(Integer.parseInt(response.getRet_code()));
                                res.setRet_msg(response.getRet_msg());
				if("0".equals(response.getRet_code())){
					mav = new ModelAndView("3ds/pago");
					mav.addObject("respuesta", "Aprobado");
					mav.addObject("idTransaccion", eM_OrderID);	
					mav.addObject("autorizacion", eM_Auth);		
					mav.addObject("importe", transaccion.getCargo());
					mav.addObject("comision", transaccion.getComision());
					mav.addObject("total", total);
					mav.addObject("descripcionCode", response.getRet_msg());
					mav.addObject("tag", response.getId_tag());
					mav.addObject("fecha", UtilsService.getFechaActual());
					String ticket = "COMPRA TAG TELEVIA AUTORIZACION: " + response.getId_order() + " TAG: " + transaccion.getDestino()
							+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
					transaccion.setBitTicket(ticket);
					transaccion.setBitStatus(1);
					transaccion.setBitNoAutorizacion(eM_Auth);
					
					actualizaBitacora(transaccion, eM_Auth, "0", 1, res);//pendiente
					enviaCorreo(transaccion, response, total, eM_Auth, eM_OrderID, eM_Merchant);
				} else {
					mav = new ModelAndView("3ds/error");
					respuesta.setIdError(-9);
					respuesta.setMsgError("Rechazo TELEVIA - ERROR: "+response.getRet_code()+ " TAG: " + transaccion.getDestino());
					String ticket = "COMPRA TAG TELEVIA ERROR: " + response.getRet_code() + " TAG: " + transaccion.getDestino()
							+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
					transaccion.setBitTicket(ticket);
					transaccion.setBitStatus(3);
					transaccion.setBitNoAutorizacion(eM_Auth);
					transaccion.setBitCodigoError(-1);
					actualizaBitacora(transaccion, eM_Auth, "-1", 0, res);//pendiente
				}
			} else {
				respuesta.setIdError(-9);
				respuesta.setMsgError("Tarjeta rechazada, " + eM_Response + " - " + eM_RefNum);
				LOGGER.error("Error Pago 3D Secure: " + eM_Response + " - " + eM_RefNum);
				String ticket = "Tarjeta rechazada  TAG: " + transaccion.getDestino()
						+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
				transaccion.setBitTicket(ticket);
				transaccion.setBitStatus(2);
				transaccion.setBitNoAutorizacion(eM_Auth);
				transaccion.setBitCodigoError(-1);
				actualizaBitacora(transaccion, eM_Auth, "-1", 0, res);//pendiente
			}
		} catch (Exception e) {
			respuesta.setIdError(-100);
			respuesta.setMsgError("Ocurrio un error, problemas internos -3");
			LOGGER.error("Error al realizar la recarga", e);
			actualizaBitacora(transaccion, eM_Auth, "-1", 0, null);
		} finally {
			if (respuesta.getIdError() != 0) {
				mav = new ModelAndView("3ds/error");
				mav.addObject("descError", respuesta.getMsgError());
			}
		}
		return mav;
	}
	
	

	private void enviaCorreo(TBitacoraVO transaccion, topup_service response, double total,
			String eM_Auth, String eM_OrderID, String eM_Merchant) {
		try {
			java.util.Date fecha = new java.util.Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"dd/MM/yyyy HH:mm:ss");
			DatosCorreoVO datosCorreoVO = new DatosCorreoVO();
			
			String emailBody = mapper.getParametro("@MENSAJE_COMPRATELEVIA");
			String eMailSubject = mapper.getParametro("@ASUNTO_COMPRATELEVIA").toString();
			datosCorreoVO.setNombre(transaccion.getNombre());
			datosCorreoVO.setUsuario(transaccion.getLogin());
			datosCorreoVO.setMonto(transaccion.getCargo());
			datosCorreoVO.setAutorizacionIave(response.getId_order());
			datosCorreoVO.setAutorizacion(eM_Auth);
			datosCorreoVO.setComision(transaccion.getComision());
			datosCorreoVO.setFecha(dateFormat.format(fecha));
			datosCorreoVO.setTagIave(transaccion.getDestino());
			datosCorreoVO.setEmail(transaccion.getMail());
			datosCorreoVO.setIdBitacora(transaccion.getIdBitacora());	
			AddCelGenericMail.generatedMailTelevia(datosCorreoVO, emailBody, eMailSubject);
			
		} catch (Exception e) {
			LOGGER.error("EnviaMail: " + e);
		}

	}

	public topup_service aprovisionaTelevia(TBitacoraVO transaccion){
		topup_service response = new topup_service();
		String iduser, idpass, resp = null;
                long idorder = 0L;
		try {
			Calendar mC = Calendar.getInstance();
			String wfecha = agregaCeros(new StringBuilder().append("").append(mC.get(5)).toString(), 2) + "/"
					+ agregaCeros(new StringBuilder().append("").append(mC.get(2) + 1).toString(), 2) + "/" + mC.get(1) + " "
					+ agregaCeros(new StringBuilder().append("").append(mC.get(11)).toString(), 2) + ":"
					+ agregaCeros(new StringBuilder().append("").append(mC.get(12)).toString(), 2) + ":"
					+ agregaCeros(new StringBuilder().append("").append(mC.get(13)).toString(), 2);
			String prefijo = "";
			String puntos = "";
			if (transaccion.getDestino().length() != 11) {
				while (transaccion.getDestino().length() < 8) {
					transaccion.setDestino("0" + transaccion.getDestino());
				}
				if ((transaccion.getDestino().length() == 8) && (Long.parseLong(transaccion.getDestino()) >= 20000000L)) {
					prefijo = "IMDM";
				}
				if ((transaccion.getDestino().length() == 8) && (Long.parseLong(transaccion.getDestino()) < 20000000L)) {
					prefijo = "CPFI";
				}
				if (transaccion.getDestino().length() == 8) {
					puntos = "..";
				}
			}
                        
                    iduser = mapper.getParametro("@ID_USER_TELEVIA");
                    idpass = mapper.getParametro("@ID_PASS_TELEVIA");
	            idorder = mapper.getMaxIdOrder();
                    
                    LOGGER.info("Numero de tag TELEVIA  : " + transaccion.getDestino());
                    
			StringBuffer pXML = new StringBuffer().append("<topup_service><request>").append("<user>").append(iduser)
				.append("</user>").append("<password>").append(idpass).append("</password>")
				.append("<id_order>").append(idorder+1).append("</id_order>")
				.append("<id_tag>").append(transaccion.getDestino()).append("</id_tag>").append("<amount>")
				.append(transaccion.getCargo()).append("</amount>").append("</request></topup_service>");

                        System.out.println("MPX COMPRA TELEVIA : " + pXML);
                        com.addcel.televiaservice.servicios.client.televia.TopupServiceSoapBindingStub binding = null;
                    try {
                        binding = (com.addcel.televiaservice.servicios.client.televia.TopupServiceSoapBindingStub) new com.addcel.televiaservice.servicios.client.televia.TopupServiceServiceLocator().getTopupService();
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
			ObjectFactory factory = new ObjectFactory();
			Reload reload = factory.createReload();
			reload.setSXML(pXML.toString());
			resp = binding.doTopupService(pXML.toString());
                        String raux = Utils.transformarXMLRes(resp);
			ParseTeleviaResponse parseTelevia = new ParseTeleviaResponse();
			response = parseTelevia.parseResponseXStream(raux);
			LOGGER.info("RESPUESTA DE TELEVIA: ID TRANSACCION: "+transaccion.getIdBitacora()
					+" AMOUNT: "+response.getAmount()+" TRX_NO: "+response.getId_order()+" AUTONO: "+response.getSeq()
                                        +" AMOUNT: "+response.getAmount()+" TRX_NO: "+response.getId_order()+" AUTONO: "+response.getBalance()
					+" RESPONSE CODE: "+response.getRet_code()+" DESCRIPCION CODE: "+response.getRet_msg());
		} catch (Exception e) {
			LOGGER.info("ERROR AL APROVISIONAR LA TRANSACCION TELEVIA: "+transaccion.getIdBitacora()+" CAUSA: "+e.getMessage());
			LOGGER.error("ERROR: "+e.getLocalizedMessage());
			response.setRet_code("-01");
			response.setRet_msg("Error al comunicarse con TELEVIA");
		}
		return response;
	}
	
	public boolean esTuTag(String idProducto) {
		String productosTuTag = null;
		String[] productosTuTagArr = null;
		boolean resultado = false;
		try {
			productosTuTag = mapper.getParametro("@PRODUCTOS_TUTAG");
			if(productosTuTag != null){
				productosTuTagArr = productosTuTag.split("\\|");
				for (String s : productosTuTagArr) {
					if (s.equalsIgnoreCase(idProducto)) {
						resultado = true;
						break;
					}
				}	
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error al validar es TU TAG: {}", e);
		}
		return resultado;
	}
	
	/**
	 * 
	 * @param json
	 * @return
	 * @deprecated
	 */
	public String compraTELEVIA(String json) {
		double comision = 0;
		TeleviaVO compra = null;
		UsuarioVO usuario = null;
		ReglasResponse response = null;
		Producto producto = null;
		AbstractVO resValidacion = null;
		try {
			json = Utils.decryptJson(json);
			compra = (TeleviaVO) jsonUtils.jsonToObject(json, TeleviaVO.class);
			LOGGER.debug("INICIANDO PROCESO DE RECARGA TELEVIA 3D SECURE - JSON {}", json);
			usuario = mapper.validaUsuario(compra.getLogin());
			if(usuario != null){
				LOGGER.debug("TELEVIA validacion Reglas negocio, idUsuario: " + usuario.getIdUsuario()+" "+ setSMS(usuario.getTarjeta()));
				response = validaReglas(usuario.getIdUsuario(), setSMS(usuario.getTarjeta()), 0);
				if(response == null){
					producto = mapper.getProductoByClaveWS(compra.getProducto());
					comision = mapper.getComisionXProducto(Integer.parseInt(producto.getProveedor()));
					compra.setComision(Double.valueOf(comision));
					insertaBitacoras(compra, usuario, producto);
					resValidacion = validaBloqueos(usuario.getTarjeta(), compra.getImei());
					if(resValidacion.getIdError() != 0){
						json =  jsonUtils.objectToJson(resValidacion);
					}
				} else {
					json =  jsonUtils.objectToJson(response);
				}
			} else {
				json = "{\"idError\": -1, \"mensajeError\": \"El usuario no existe.\"}";
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("COMPRA TELEVIA 3D SECURE - CAUSA: "+e.getCause());
		} finally {
			json = Utils.encryptJson(json); 
		}
		return json;
	}

	private void actualizaBitacora(TBitacoraVO bitacora , String eM_Auth, String string,
			int i, TBitTeleviaDet response) {
		try {
			mapper.actualizaBitacora(bitacora);
			if(response != null){
				response.setId_bitacora((int)bitacora.getIdBitacora());
				mapper.actualizaBitacoraTELEVIA(response);//pendiente
				bitacora.setTarjetaTelevia(response.getId_order()+"-"+response.getSeq()); //pendiente
			}
			mapper.actualizaBitacoraProsa(bitacora);
		} catch (Exception e) {
			LOGGER.error("ERROR AL ACTUALIZAR LAS BITACORAS - CAUSA: "+e.getCause());
			e.printStackTrace();
		}
	}

	private String agregaCeros(String cad, int NumCeros) {
		String tmp = "00000000000000000000000000000";
		return (tmp + cad).substring((tmp + cad).length() - NumCeros, (tmp + cad).length());
	}
	
	private AbstractVO validaBloqueos(String tarjeta, String imei) {
		AbstractVO response = new AbstractVO();
		int bloqueoTag, bloqueoImei = 0;
		bloqueoTag = mapper.getInfoTag(tarjeta);
		bloqueoImei = mapper.getInfoImei(imei);
		if(bloqueoTag != 0){
			response.setIdError(-1);
			response.setMensajeError("El tag se encuentra bloqueado.");
		} else if(bloqueoImei != 0){
			response.setIdError(-1);
			response.setMensajeError("El imei se encuentra bloqueado.");
		}
		return response;
	}

	private void insertaBitacoras(TeleviaVO compra, UsuarioVO usuario, Producto producto) {
		TBitacoraVO bitacora = new TBitacoraVO();
                TBitTeleviaDet bitdet = new TBitTeleviaDet();
		bitacora.setTipo(compra.getTipo());
		bitacora.setSoftware(compra.getSoftware());
		bitacora.setModelo(compra.getModelo());
		bitacora.setWkey(compra.getKey());
		bitacora.setIdUsuario(String.valueOf(usuario.getIdUsuario()));
		bitacora.setIdProducto(Integer.valueOf(producto.getClave()));
		bitacora.setCar_id(Integer.parseInt(producto.getClaveWS()));
		bitacora.setCargo(Double.parseDouble(producto.getMonto()));
		bitacora.setIdProveedor(Integer.valueOf(producto.getProveedor()));
		bitacora.setConcepto("Compra TELEVIA -  TAG: " + compra.getTarjeta() + " "
				+ producto.getMonto() + " Comision: " + compra.getComision());
		bitacora.setImei(compra.getImei());
		bitacora.setDestino(compra.getTarjeta());
		bitacora.setTarjeta_compra(usuario.getNumTarjeta());
		bitacora.setAfiliacion(Constantes.MERCHANT);
		bitacora.setComision(compra.getComision());
		bitacora.setCx(compra.getCx());
		bitacora.setCy(compra.getCy());
		bitacora.setPin(compra.getPin());
		mapper.addBitacora(bitacora);
                bitdet.setId_bitacora((int)bitacora.getIdBitacora());
                bitdet.setId_order(Long.MIN_VALUE);
		mapper.addBitacoraTELEVIA(bitdet);
		mapper.addBitacoraProsa(bitacora);
		compra.setIdBitacora(bitacora.getIdBitacora());
	}

	private ReglasResponse validaReglas(long idUsuario, String tarjeta, int idProducto) {
		ReglasResponse response = null;
		String numUsuario = null;
		String montoUsuario = null;
		String numTarjeta = null;
		String montoTarjeta = null;

		RuleVO valoresU = null;
		RuleVO valoresT = null;
		try {
			numUsuario = mapper.getParametro("@IMASD_NUM_TRAN_USUARIO");
			montoUsuario = mapper.getParametro("@IMASD_MONTO_USUARIO");
			numTarjeta = mapper.getParametro("@IMASD_NUM_TRAN_TARJETA");
			montoTarjeta = mapper.getParametro("@IMASD_MONTO_TARJETA");

			valoresU = mapper.selectRule(idUsuario, null, "5,8,17", "@IMASD_DIAS_TRAN_USUARIO");
			valoresT = mapper.selectRule(0L, tarjeta, "5,8,17", "@IMASD_DIAS_TRAN_TARJETA");
			
			if (((Integer) valoresU.getNumero()).doubleValue() >= Integer.valueOf(numUsuario)) {
				response = new ReglasResponse(0, "920",
						"En el periodo " + valoresU.getFechaInicio() + " a " + valoresU.getFechaFin()
								+ " a superado el número maximo de transacciones permitidas: " + numUsuario);
			} else if (((Double) valoresU.getMonto()).doubleValue() >= Double.valueOf(montoUsuario)) {
				response = new ReglasResponse(0, "921",
						"En el periodo " + valoresU.getFechaInicio() + " a " + valoresU.getFechaFin()
								+ " a superado el Monto maximo permitido: $ " + formatoImporte(Double.valueOf(montoUsuario)));
			} else if (((Integer) valoresT.getNumero()).doubleValue() >= Integer.valueOf(numTarjeta)) {
				response = new ReglasResponse(0, "922",
						"En el periodo " + valoresT.getFechaInicio() + " a " + valoresT.getFechaFin()
								+ " a superado el número maximo de transacciones permitidas para una Tarjeta: "
								+ numTarjeta);
			} else if (((Double) valoresT.getMonto()).doubleValue() >= Double.valueOf(montoTarjeta)) {
				response = new ReglasResponse(0, "923",
						"En el periodo " + valoresT.getFechaInicio() + " a " + valoresT.getFechaFin()
								+ " a superado el Monto maximo permitido para una Tarjeta: $ "
								+ formatoImporte(Double.valueOf(montoTarjeta)));
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error al validar Reglas de Negocio.", e);
			response = new ReglasResponse(0, "920", "Ocurrio un error al validar Reglas de Negocio.");
		}
		return response;
	}

	

    public static String setSMS(String Telefono){
        return Crypto.aesEncrypt(parsePass(TELEFONO_SERVICIO),  Telefono);    
    }
	
    public static String parsePass(String pass){
        int len = pass.length();
        String key = "";

        for (int i =0; i < 32 /len; i++){
              key += pass;
        }

        int carry = 0;
        while (key.length() < 32){
              key += pass.charAt(carry);
              carry++;
        }
        return key;
    }
    
    public static String formatoImporte(double numDouble){
        String total = null;
        try{
            total = DF.format(numDouble);
        }catch(Exception e){
            LOGGER.error("Error en formatoImporte: "+ e.getMessage());
        }
        return total;
    }

}
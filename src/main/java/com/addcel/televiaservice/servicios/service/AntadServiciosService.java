package com.addcel.televiaservice.servicios.service;

import static com.addcel.televiaservice.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;
import static com.addcel.televiaservice.servicios.utils.Constantes.JSON_ERROR_INESPERADO;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_ERROR_CONSULTA_SERVICIOS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_ERROR_GET_CATEGORIAS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_ERROR_GET_RECARGAS_MONTOS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_ERROR_GET_RECARGAS_SERVICIOS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_ERROR_PROCESA_PAGO;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_ERROR_REALIZA_CONSULTA;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_SERVICIOS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_PROCESO_PROCESA_PAGO;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_PROCESO_REALIZA_CONSULTA;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_SERVICIOS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_RESPUESTA_GET_CATEGORIAS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_RESPUESTA_GET_RECARGAS_MONTOS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_RESPUESTA_GET_RECARGAS_SERVICIOS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_RESPUESTA_PREALIZA_CONSULTA;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_RESPUESTA_PROCESA_PAGO;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_SERVICE;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_SERVICE_GET_CATEGORIAS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_SERVICE_GET_RECARGAS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_SERVICE_GET_RECARGAS_MONTOS;
import static com.addcel.televiaservice.servicios.utils.Constantes.LOG_SERVICE_GET_RECARGAS_SERVICIOS;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.addcel.televiaservice.servicios.client.antad.Consulta;
import com.addcel.televiaservice.servicios.client.antad.ConsultaResponse;
import com.addcel.televiaservice.servicios.client.antad.ObjectFactory;
import com.addcel.televiaservice.servicios.client.antad.ProcesaAutorizacion;
import com.addcel.televiaservice.servicios.client.antad.ProcesaAutorizacionResponse;
import com.addcel.televiaservice.servicios.model.mapper.AntadServiciosMapper;
import com.addcel.televiaservice.servicios.model.vo.Categorias;
import com.addcel.televiaservice.servicios.model.vo.RecargaMontos;
import com.addcel.televiaservice.servicios.model.vo.Recargas;
import com.addcel.televiaservice.servicios.model.vo.RecargasResponse;
import com.addcel.televiaservice.servicios.model.vo.ServiciosAntadRequest;
import com.addcel.televiaservice.servicios.model.vo.ServiciosAntadResponse;
import com.addcel.televiaservice.servicios.model.vo.ServiciosResponse;
import com.addcel.televiaservice.servicios.model.vo.Token;
import com.addcel.televiaservice.servicios.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class AntadServiciosService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AntadServiciosService.class);

	private static final SimpleDateFormat SDF = new SimpleDateFormat(FORMATO_FECHA_ENCRIPT);
	
	@Autowired
	private AntadServiciosMapper mapper;
	
	@Autowired
	private UtilsService jsonUtils;
	
    @Autowired
    @Qualifier("webServiceAntadBridge")
    private transient WebServiceTemplate webServiceTemplate;
	
	private static final String SUCCESS = "Success";
	
	private static final String UNSUCCESS = "Unsuccess";
	
	public String consultaServicios(String json) {
		List<ServiciosAntadResponse> serviciosAntadList = null;
		ServiciosResponse response = new ServiciosResponse();
		ServiciosAntadRequest request = null;
		try {
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_SERVICIOS + json);
			request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			serviciosAntadList = mapper.consultaServiciosAntad(request);
			response.setServicios(serviciosAntadList);
			response.setMensajeError(SUCCESS);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_SERVICIOS+"["+e.getCause()+"]");
			response.setMensajeError(UNSUCCESS+" CAUSA: "+e.getCause());
		} finally{
			json = (String) jsonUtils.objectToJson(response);
			LOGGER.info(LOG_RESPUESTA_CONSULTA_SERVICIOS+json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return json;
	}

	public String realizaAprovisionamiento(String json) {
		ProcesaAutorizacion autorizacion = null;
		ProcesaAutorizacionResponse response = null;
		ObjectFactory factory = new ObjectFactory();
		try {
			json = procesaCifrado(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_PROCESA_PAGO + json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
			autorizacion = new ProcesaAutorizacion();
			autorizacion.setJson(json);
			response = (ProcesaAutorizacionResponse) webServiceTemplate.marshalSendAndReceive(
					factory.createProcesaAutorizacion(autorizacion));
			json = procesaCifrado(response.getReturn());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_PROCESA_PAGO+" ["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_PROCESA_PAGO+json);
			json = cifraRespuesta(json);
		}
		return json;
	}

	public String realizaConsulta(String json) {
		ObjectFactory factory = new ObjectFactory();
		Consulta consulta;
		ConsultaResponse response; 
		try {
			json = procesaCifrado(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_REALIZA_CONSULTA + json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
			consulta = new Consulta();
			consulta.setJson(json);
			response = (ConsultaResponse) webServiceTemplate.marshalSendAndReceive(factory.createConsulta(consulta));
			json = procesaCifrado(response.getReturn());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_REALIZA_CONSULTA+"["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_PREALIZA_CONSULTA + json);
			json = cifraRespuesta(json);
		}
		return json;
	}

	public String getToken() {
		Token respuesta = new Token();
		String token;
		String json = null; 
		try {
			token = mapper.getFechaActual();
			token = AddcelCrypto.encryptHard(token);
			LOGGER.info("TOKEN GENERADO: "+token);
			respuesta.setToken(token);
			respuesta.setMensajeError("Token exitoso");
		} catch (Exception e) {
			LOGGER.error("ERROR AL CONSULTAR EL TOKEN EN BD - CAUSA: ["+e.getCause()+"]");
			respuesta.setMensajeError("No fue posible consultar el token en BD");
			respuesta.setIdError(-1);
		} finally{
			json = (String) jsonUtils.objectToJson(respuesta);
			json = cifraRespuesta(json);
			LOGGER.info("CONSULTA DE TOKEN FINALIZADA");
		}
		return json;
	}
	
	public String getRecargas(String json, String id) {
		List<Recargas> recargasAntadList = null;
		ServiciosAntadRequest request = null;
		RecargasResponse response = new RecargasResponse();
		try {
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE_GET_RECARGAS, id, json);
			request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			recargasAntadList = mapper.getRecargas(request.getIdAplicacion());
			response.setMensajeError(SUCCESS);
			response.setRecargas(recargasAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_SERVICIOS+"["+e.getCause()+"]");
			response.setMensajeError(UNSUCCESS+" CAUSA: "+e.getCause());
		} finally{
			json = (String) jsonUtils.objectToJson(response);
			LOGGER.info(LOG_RESPUESTA_CONSULTA_SERVICIOS+json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return json;
	}
	
	public String getRecargaServicios(String json, String id) {
		List<Recargas> serviciosAntadList = null;
		ServiciosAntadRequest request = null;
		RecargasResponse response = new RecargasResponse();
		try {
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE_GET_RECARGAS_SERVICIOS, id, json);
			request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			serviciosAntadList = mapper.getRecargaServicios(request.getIdAplicacion());
			response.setMensajeError(SUCCESS);
			response.setServicios(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_GET_RECARGAS_SERVICIOS+"["+e.getCause()+"]");
			response.setMensajeError(UNSUCCESS+" CAUSA: "+e.getCause());
		} finally{
			json = (String) jsonUtils.objectToJson(response);
			LOGGER.info(LOG_RESPUESTA_GET_RECARGAS_SERVICIOS+json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return json;
	}
	
	public String getRecargaMontos(String json, String id) {
		List<RecargaMontos> montosAntadList = null;
		ServiciosAntadRequest request = null;
		RecargasResponse response = new RecargasResponse();
		try {
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE_GET_RECARGAS_MONTOS, id, json);
			request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			montosAntadList = mapper.getRecargaMontos(request.getIdServicio());
			response.setMensajeError(SUCCESS);
			response.setMontos(montosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_GET_RECARGAS_MONTOS, id, "["+e.getCause()+"]");
			response.setMensajeError(UNSUCCESS+" CAUSA: "+e.getCause());
		} finally{
			json = (String) jsonUtils.objectToJson(response);
			LOGGER.info(LOG_RESPUESTA_GET_RECARGAS_MONTOS, json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return json;
	}
	
	public String getServicioCategorias(String json, String id) {
		List<Categorias> categoriasAntadList = null;
		ServiciosAntadRequest request = null;
		RecargasResponse response = new RecargasResponse();
		try {
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE_GET_CATEGORIAS, id, json);
			request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			categoriasAntadList = mapper.getServicioCategorias(request.getIdAplicacion());
			response.setMensajeError(SUCCESS);
			response.setCategorias(categoriasAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_GET_CATEGORIAS, id, "["+e.getCause()+"]");
			response.setMensajeError(UNSUCCESS+" CAUSA: "+e.getCause());
		} finally{
			json = (String) jsonUtils.objectToJson(response);
			LOGGER.info(LOG_RESPUESTA_GET_CATEGORIAS, json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return json;
	}
	
	private static String cifraRespuesta(String json){
		return AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
	}
	
	private static String procesaCifrado(String json){
		return AddcelCrypto.decryptSensitive(json);
	}
	
	public WebServiceTemplate getWebServiceTemplate() {
		return webServiceTemplate;
	}

	public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
		this.webServiceTemplate = webServiceTemplate;
	}

	

}
